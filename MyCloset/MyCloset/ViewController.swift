//
//  ViewController.swift
//  MyCloset
//
//  Created by ronak patel on 12/03/18.
//  Copyright © 2018 YDSHaridham. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import Alamofire
import GoogleSignIn

class ViewController: UIViewController, GIDSignInUIDelegate {

    
    var signInWith:String = ""
    @IBOutlet var btnLogin:UIButton!
    @IBOutlet var btnSignUp:UIButton!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        
        GIDSignIn.sharedInstance().uiDelegate = self
        UserDefaults.standard.set(false, forKey: "isFirstTime")
        if !UserDefaults.standard.bool(forKey: "isFirstTime") {
            UserDefaults.standard.set(true, forKey: "isFirstTime")
            let guideVC = self.storyboard?.instantiateViewController(withIdentifier: "GuideViewController") as! GuideViewController
            guideVC.isPresentView = true
            self.present(guideVC, animated: true, completion: nil)
        }
        
        //-------- NotificationCenter Register ------
        NotificationCenter.default.addObserver(self, selector: #selector(getGoogleUserData(notification:)), name: .signInWithGoogle, object: nil)
        
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Button Tapped Event
    @IBAction func tappedOnFacebook(_ sender:UIButton) {
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logIn(withReadPermissions: ["email","public_profile"], from: self) { (result, error) -> Void in
            if (error == nil){
                let fbloginresult : FBSDKLoginManagerLoginResult = result!
                if let fbPermissions = fbloginresult.grantedPermissions {
                    if(fbPermissions.contains("email"))
                    {
                        self.getFBUserData()
                    }
                }
                
            }
        }
    }
    
    @IBAction func tappedOnGoogle(_ sender:UIButton) {
        GIDSignIn.sharedInstance().signIn()
    }
    
    @IBAction func tappedOnLogin(_ sender:UIButton) {
        let loginView = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.navigationController?.pushViewController(loginView, animated: true)
        
//        let startColor = UIColor.init(red: 36.0/255.0, green: 32.0/255.0, blue: 31.0/255.0, alpha: 1.0)
//        let middleColor = UIColor.init(red: 97.0/255.0, green: 84.0/255.0, blue: 81.0/255.0, alpha: 1.0)
//        let endColor = UIColor.init(red: 177.0/255.0, green: 174.0/255.0, blue: 172.0/255.0, alpha: 1.0)
//        self.btnLogin.applyGradient(colours: [startColor, middleColor, endColor], locations: [0.0, 0.5, 1.0])
    }
    
    @IBAction func tappedOnRegister(_ sender:UIButton) {
        let registerView = self.storyboard?.instantiateViewController(withIdentifier: "RegisterViewController") as! RegisterViewController
        self.navigationController?.pushViewController(registerView, animated: true)
    }
    
    //MARK: - Get Facebook User Details
    func getFBUserData(){
        if((FBSDKAccessToken.current()) != nil){
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    //everything works print the user data
                    print(result ?? "")
                    let fbData = result as! [String:Any]
                    self.signInWith = "Facebook"
                    setMyUserDefaults(value: fbData, key: MyUserDefaults.FBdata)
                    self.isUserExist(strEmail: fbData["email"] as! String)
                }
            })
        }
    }
    
    @objc func getGoogleUserData(notification:NSNotification){
        if let googleData = getMyUserDefaults(key: MyUserDefaults.googleData) as? [String:Any]  {
            self.signInWith = "Google"
            self.isUserExist(strEmail: googleData["email"] as! String)
        }
    }
    
    
    func isUserExist(strEmail:String) {
        let parameters = ["i_key":WebURL.appKey,
                          "email":strEmail] as [String : Any]
        appDelegate.showHud()
        
        request(WebURL.emailVerification, method: .post, parameters: parameters).responseJSON(completionHandler: { (response) in
            print("emailVerification Result:\(response)")
            appDelegate.hideHud()
            if response.result.isSuccess {
                if let dictResult:[String:Any] = response.result.value as! [String : Any]? {
                    if (dictResult["o_returnCode"] as! Bool){
                        setMyUserDefaults(value: true, key: MyUserDefaults.isLogin)
                        setMyUserDefaults(value: dictResult["o_data"] as! [String:Any], key: MyUserDefaults.UserData)
                        appDelegate.updateDeviceToken(strToken: getMyUserDefaults(key: MyUserDefaults.FCMDeviceToken) as! String)
                        self.presentingViewController?.dismiss(animated: true, completion: nil)
                    }else{
                        /*setMyUserDefaults(value: false, key: MyUserDefaults.isLogin)
                        let registerView = self.storyboard?.instantiateViewController(withIdentifier: "RegisterViewController") as! RegisterViewController
                        registerView.strSignInWith = self.signInWith
                        self.navigationController?.pushViewController(registerView, animated: true)
                        //let errorMsg = dictResult["o_message"] as! String
                        //appDelegate.showAlertMessage(strMessage: errorMsg)*/
                    }
                }
            }
            //self.presentingViewController?.dismiss(animated: true, completion: nil)
        })
    }
    
    //MARK: - GIDSignInUIDelegate
    func sign(inWillDispatch signIn: GIDSignIn!, error: Error!) {
        
    }
    
    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }
    
    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
        self.dismiss(animated: true, completion: nil)
    }
    


}

