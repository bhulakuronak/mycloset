//
//  AppDelegate.swift
//  MyCloset
//
//  Created by ronak patel on 12/03/18.
//  Copyright © 2018 YDSHaridham. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import FBSDKCoreKit
import GoogleSignIn
import Google
import Firebase
import UserNotifications
import Alamofire
import CoreLocation
import Stripe
import Fabric
import Crashlytics
import IQKeyboardManagerSwift

let publishableKey = "pk_test_P3Pd4IO4RVo8kPl5qsUaEqbb"
let appDelegate = UIApplication.shared.delegate as! AppDelegate

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, GIDSignInDelegate, UNUserNotificationCenterDelegate, CLLocationManagerDelegate {
    
    var mbProgressHud:MBProgressHUD!
    var locationManager:CLLocationManager!
    var strCityName = ""
    
    var window: UIWindow?
    


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        IQKeyboardManager.shared.enable = true
        
        Fabric.with([Crashlytics.self])
        
        UIApplication.shared.statusBarView?.backgroundColor = UIColor(patternImage: UIImage(named: "imgTopBar")!) //Color.COLOR_NAVIGATION_BACKGROUND
        UINavigationBar.appearance().setBackgroundImage(UIImage(named:"imgTopBar"), for: .default)
        
        setMyUserDefaults(value: "", key: MyUserDefaults.FilterGender)
        setMyUserDefaults(value: "", key: MyUserDefaults.FilterCategory)
        setMyUserDefaults(value: "", key: MyUserDefaults.FilterType)
        setMyUserDefaults(value: "", key: MyUserDefaults.FilterCondition)
        setMyUserDefaults(value: "", key: MyUserDefaults.FilterSize)
        setMyUserDefaults(value: "", key: MyUserDefaults.FilterTrade)
        setMyUserDefaults(value: "", key: MyUserDefaults.FilterBrand)
        UserDefaults.standard.set(false, forKey: MyUserDefaults.isFilterApply)
        //-----------------------------------Farebase Configure-------------------------------------
        FIRApp.configure()
        
        //----------PayPal------------
        PayPalMobile .initializeWithClientIds(forEnvironments:
            [PayPalEnvironmentProduction: "AdKsIxy0Nzi5hr4dB9ON9w-Ts_c1Wd3D75sz89vSK12mpvDT0JI2_4qFQOmckYkJjlvfBY22Ry9n7HST",
             PayPalEnvironmentSandbox: "AT-ySRBJvqmh74DMJgzbzovc8SUEK8HU2beYR2dqoKUWersn9294sNMJLM9V"])
        
        
        //----------Strip Intialize------------
        STPPaymentConfiguration.shared().publishableKey = publishableKey
        STPPaymentConfiguration.shared().companyName = "MyCloset"
        MainAPIClient.shared.baseURLString = WebURL.baseURL
        
        //----------Facebook Intialize------------
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
        //----------Google Intialize------------
        var configureError: NSError?
        GGLContext.sharedInstance().configureWithError(&configureError)
        assert(configureError == nil, "Error configuring Google services: \(String(describing: configureError))")
        
        GIDSignIn.sharedInstance().delegate = self
        
        //----------Register Push Notification------------
        self.registerForRemoteNotification()
        // Add observer for InstanceID token refresh callback.
        NotificationCenter.default.addObserver(self, selector: #selector(self.tokenRefreshNotification),
                                               name: NSNotification.Name.firInstanceIDTokenRefresh,
                                               object: nil)
        
        //----------Current Location------------
        self.getCurrentLocation()
        
        if (UserDefaults.standard.bool(forKey: "isLogin")) {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            
            let mainViewController = storyboard.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
            let leftViewController = storyboard.instantiateViewController(withIdentifier: "LeftViewController") as! LeftViewController
            
            
            let nvc: NavigationController = NavigationController(rootViewController: mainViewController)
            
            //UINavigationBar.appearance().tintColor = UIColor(hex: "689F38")
            
            leftViewController.mainViewController = nvc
            
            
            let slideMenuController = ExSlideMenuController(mainViewController: nvc, leftMenuViewController: leftViewController)
            slideMenuController.automaticallyAdjustsScrollViewInsets = true
            slideMenuController.delegate = mainViewController as SlideMenuControllerDelegate
            self.window?.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
            self.window?.rootViewController = slideMenuController
            self.window?.makeKeyAndVisible()
        }
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        
        var handled:Bool = FBSDKApplicationDelegate.sharedInstance().application(app, open: url, options: options)
        if #available(iOS 9.0, *) {
            handled = handled ? handled : GIDSignIn.sharedInstance().handle(url, sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String, annotation: options[UIApplicationOpenURLOptionsKey.annotation])
            return handled
        } else {
            // Fallback on earlier versions
        }
        return handled
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        FIRInstanceID.instanceID().setAPNSToken(deviceToken, type: FIRInstanceIDAPNSTokenType.prod)
        
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        print("Device Token:- \(deviceTokenString)")
        //UserDefaults.standard.setValue(deviceTokenString, forKey: "DeviceToken")
        setMyUserDefaults(value: deviceTokenString, key: MyUserDefaults.DeviceToken)
        
        if let refreshedToken = FIRInstanceID.instanceID().token() {
            print("InstanceID token: \(refreshedToken)")
            setMyUserDefaults(value: refreshedToken, key: MyUserDefaults.FCMDeviceToken)
        }
        
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        setMyUserDefaults(value: "", key: MyUserDefaults.DeviceToken)
    }
    
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        //completionHandler([.alert, .badge, .sound])
        print("Notification willPresent:\(notification.request.content.userInfo)")
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print("Notification didReceiveRemoteNotification:\(userInfo)")
        
    }
    
    
    
    //MARK: - Register RemoteNotification
    func registerForRemoteNotification() {
        if #available(iOS 10.0, *) {
            let center  = UNUserNotificationCenter.current()
            center.delegate = self
            center.requestAuthorization(options: [.sound, .alert, .badge]) { (granted, error) in
                if error == nil{
                    UIApplication.shared.registerForRemoteNotifications()
                }
            }
            //FIRMessaging.messaging().remoteMessageDelegate = self as! FIRMessagingDelegate
        }
        else {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.sound, .alert, .badge], categories: nil))
            UIApplication.shared.registerForRemoteNotifications()
        }
    }
    
    @objc func tokenRefreshNotification(notification: NSNotification) {
        if let refreshedToken = FIRInstanceID.instanceID().token() {
            print("InstanceID token: \(refreshedToken)")
            setMyUserDefaults(value: refreshedToken, key: MyUserDefaults.FCMDeviceToken)
            /*let alertController = UIAlertController(title: "InstanceID", message: "", preferredStyle: .alert)
             alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
             
             alertController.addTextField(configurationHandler: {(textField : UITextField!) -> Void in
             textField.keyboardType = UIKeyboardType.emailAddress;
             textField.text = refreshedToken
             })
             
             window?.rootViewController?.present(alertController, animated: true, completion: nil)*/
        }
    }

    //MARK: - GIDSignInDelegate
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if (error == nil) {
            // Perform any operations on signed in user here.
            /*print("UserID:\(user.userID)")                  // For client-side use only!
             print("Token:\(user.authentication.idToken)") // Safe to send to the server
             print("Profile:\(user.profile.name)")
             print("GivenName:\(user.profile.givenName)")
             print("familyName:\(user.profile.familyName)")
             print("Email:\(user.profile.email)")
             print("ProfilePic:\(user.profile.imageURL(withDimension: 200))")*/
            var googleData = [String:Any]()
            googleData["userID"] = user.userID
            //googleData["Token"] = user.authentication.idToken
            googleData["Profile"] = user.profile.name
            googleData["GivenName"] = user.profile.givenName
            googleData["familyName"] = user.profile.familyName
            googleData["email"] = user.profile.email
            googleData["profilePic"] = user.profile.imageURL(withDimension: 200).absoluteString
            setMyUserDefaults(value: googleData, key: MyUserDefaults.googleData)
            NotificationCenter.default.post(name: .signInWithGoogle, object: nil)
        } else {
            print("\(error.localizedDescription)")
            NotificationCenter.default.post(name: .signInWithGoogle, object: nil)
        }
    }
    
    //MARK: - HUD method
    func showHud()  {
        
        if let app = UIApplication.shared.delegate as? AppDelegate, let window = app.window {
            
            self.mbProgressHud = MBProgressHUD.showAdded(to: window, animated: true)
            self.mbProgressHud.label.text = "Please wait..."
            
            
            //self.mbProgressHud.hideAnimated(true, afterDelay: 30.0)
        }
    }
    
    func hideHud() {
        if self.mbProgressHud != nil {
            self.mbProgressHud.hide(animated: true)
        }
    }
    
    func showAlertMessage(strMessage:String) {
        UIAlertView(title: "SolvoList", message: strMessage, delegate: nil, cancelButtonTitle: "Okay").show()
    }
    
    func updateDeviceToken(strToken:String) {
        let userDetail = getMyUserDefaults(key: MyUserDefaults.UserData) as! [String:Any]
        
        
        let parameters = ["i_key":WebURL.appKey,
                          "device_token":strToken,
                          "device_type":"ios",
                          "user_id":userDetail["user_id"] ?? ""] as [String : Any]
        appDelegate.showHud()
        request(WebURL.updateDevicetoken, method: .post, parameters: parameters).responseJSON(completionHandler: { (response) in
            print("updateDevicetoken Result:\(response)")
            appDelegate.hideHud()
            if response.result.isSuccess {
                if let dictResult:[String:Any] = response.result.value as! [String : Any]? {
                    print(dictResult)
                }
            }
            //self.presentingViewController?.dismiss(animated: true, completion: nil)
        })
    }
    
    func sendPushMessage(strToken:String,deviceType:String,dictBody:[String:String]) {
        _ = getMyUserDefaults(key: MyUserDefaults.UserData) as! [String:Any]
        
        var strKey = "notification"
        if deviceType == "android" {
            strKey = "data"
        }
        let parameters = ["to":strToken,
                          "priority":"high",
                          strKey:dictBody] as [String : Any]
        let headerHttp = ["Authorization":"key=AIzaSyCCpAd1-hDrIDEXD63IC1k9ouz-h3an_vs",
                          "Content-Type":"application/json"]
        print("Parameter:\(parameters)")
        //appDelegate.showHud()
        request("https://fcm.googleapis.com/fcm/send", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headerHttp).responseJSON(completionHandler: { (response) in
            print("send message Result:\(response)")
            //appDelegate.hideHud()
            if response.result.isSuccess {
                if let dictResult:[String:Any] = response.result.value as! [String : Any]? {
                    print(dictResult)
                }
            }
        })
    }
    
    //MARK: - Get Current location
    func getCurrentLocation() {
        locationManager = CLLocationManager()
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        //locationManager.startMonitoringSignificantLocationChanges()
    }
    
    func getCurrentCity() {
        if self.strCityName != "" {
            return
        }
        
        if let latitude = getMyUserDefaults(key: MyUserDefaults.UserLatitude) as? CLLocationDegrees {
            let longitude = getMyUserDefaults(key: MyUserDefaults.UserLongitude) as! CLLocationDegrees
            let location = CLLocation(latitude: latitude, longitude: longitude)
            CLGeocoder().reverseGeocodeLocation(location, completionHandler: { (placemarks, error) in
                if error != nil {
                    print("Reverse geocoder failed with error" + error!.localizedDescription)
                    return
                }
                
                if placemarks!.count > 0 {
                    let pm = placemarks![0]
                    
                    print(pm.postalCode ?? "") //print zip code
                    print(pm.locality ?? "") //print City
                    self.strCityName = pm.locality!
                    setMyUserDefaults(value: self.strCityName, key: MyUserDefaults.currentCityName)
                }
                else {
                    print("Problem with the data received from geocoder")
                }
            })
        }
    }
    
    //MARK: - CLLocationManagerDelegate
    func locationManager(_ manager: CLLocationManager, didStartMonitoringFor region: CLRegion) {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        print("didStartMonitoringFor = \(locValue.latitude) \(locValue.longitude)")
        setMyUserDefaults(value: locValue.latitude, key: MyUserDefaults.UserLatitude)
        setMyUserDefaults(value: locValue.longitude, key: MyUserDefaults.UserLongitude)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        //print("didUpdateLocations = \(locValue.latitude) \(locValue.longitude)")
        setMyUserDefaults(value: locValue.latitude, key: MyUserDefaults.UserLatitude)
        setMyUserDefaults(value: locValue.longitude, key: MyUserDefaults.UserLongitude)
        self.getCurrentCity()
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        switch CLLocationManager.authorizationStatus()
        {
        case .authorizedAlways:
            print("Location services authorised by user");
            break;
            
        case .authorizedWhenInUse:
            print("Location services authorised by user");
            break;
            
        case .denied:
            print("Location services denied by user");
            break;
            
        case .restricted:
            print("Parental controls restrict location services");
            break;
            
        case .notDetermined:
            print("Unable to determine, possibly not available");
            break;
            
        default:
            print("Default");
            break;
        }
    }
    

}

extension UIApplication {
    var statusBarView: UIView? {
        if responds(to: Selector("statusBar")) {
            return value(forKey: "statusBar") as? UIView
        }
        return nil
    }
}
