//
//  BrandViewController.swift
//  MyCloset
//
//  Created by M on 16/03/18.
//  Copyright © 2018 YDSHaridham. All rights reserved.
//

import UIKit

@objc protocol SelectBrandProtocol {
    @objc func selectBrand(brandName:String, brandId:String)
    @objc optional func selectMultipleBrand(arrBrand:[Any])
}

class BrandViewController: UIViewController, UITableViewDataSource, UITableViewDelegate,UISearchBarDelegate {
    
    var delegate:SelectBrandProtocol!
    var isSelectMultiple:Bool = false
    @IBOutlet var seachBar:UISearchBar!
    @IBOutlet var tblList:UITableView!
    var arrBrandList = [Any]()
    var arrSearchBrandList = [Any]()
    var arrSelectedBrand = [Any]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //  Converted to Swift 4 by Swiftify v4.1.6640 - https://objectivec2swift.com/
        if let path = Bundle.main.path(forResource: "brand_list", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                arrBrandList = jsonResult as! [Any]
                arrSearchBrandList = arrBrandList
                self.tblList.reloadData()
            } catch {
                print(error)
                // handle error
            }
        }
        
        //tblList.register(UITableViewCell.self, forCellReuseIdentifier: "DefaultCell")
        
        //------Navigation Button------
        self.title = "Account"
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "imgBack")?.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(tappedOnBack(_:)))
        
        if self.isSelectMultiple {
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(tappedOnDone(_:)))
        }
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Button Tapped Event
    @IBAction func tappedOnBack(_ sender:UIBarButtonItem)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func tappedOnDone(_ sender:UIBarButtonItem)
    {
        self.delegate.selectMultipleBrand!(arrBrand: arrSelectedBrand)
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //MARK: - UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrSearchBrandList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        let dict = self.arrSearchBrandList[indexPath.row] as! [String:String]
        cell?.textLabel?.text = dict["Name"]
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if isSelectMultiple {
            if let cell = tableView.cellForRow(at: indexPath as IndexPath) {
                let dict = self.arrSearchBrandList[indexPath.row] as! [String:String]
                if cell.accessoryType == .checkmark{
                    cell.accessoryType = .none
                    let index = arrSelectedBrand.index(where: { $0 as! [String:String] == dict })
                    arrSelectedBrand.remove(at: index!)
                }else{
                    cell.accessoryType = .checkmark
                    let contains = arrSelectedBrand.contains(where: { $0 as! [String:String] == dict })
                    if !contains {
                        arrSelectedBrand.append(dict)
                    }
                }
            }
            
            
        }else{
            if let cell = tableView.cellForRow(at: indexPath as IndexPath) {
                if cell.accessoryType == .checkmark{
                    cell.accessoryType = .none
                }else{
                    cell.accessoryType = .checkmark
                }
            }
            let dict = self.arrSearchBrandList[indexPath.row] as! [String:String]
            self.delegate.selectBrand(brandName: dict["Name"]!,brandId: dict["Id"]!)
            self.navigationController?.popViewController(animated: true)
        }
        
        
    }
    
    //MARK: - UISearchBarDelegate
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        //----------------------------------------------------------------------
        //searchBar.frame = CGRect(x:0, y:0, width:ScreenSize.SCREEN_WIDTH - 100, height:20)
        searchBar.setShowsCancelButton(true, animated: true)
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        //-----------------------------------------------------------------------
        //searchBar.frame = CGRect(x:0, y:0, width:200, height:20)
        searchBar.setShowsCancelButton(false, animated: true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        if searchBar.text == "" {
            arrSearchBrandList = arrBrandList;
            self.tblList.reloadData()
        }
        
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print("search text: \(searchText)")
        let namePredicate = NSPredicate(format: "Name CONTAINS[C] '\(searchText)'")
        arrSearchBrandList = self.arrBrandList.filter { namePredicate.evaluate(with: $0) }
        if searchText == "" {
            arrSearchBrandList = arrBrandList;
        }
        self.tblList.reloadData()
        print("search result: \(arrSearchBrandList)")
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
