//
//  AddProductVC.swift
//  MyCloset
//
//  Created by M on 14/03/18.
//  Copyright © 2018 YDSHaridham. All rights reserved.
//

import UIKit

class AddProductVC: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {

    var imgProdCoverImage:UIImage!
    @IBOutlet var imageViewProdCover:UIImageView!
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imageViewProdCover.image = imgProdCoverImage
        
        //------Navigation Button------
        
        
        
        //self.navigationController?.navigationBar.barTintColor = Color.COLOR_NAVIGATION_BACKGROUND
        self.title = "Account"
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "imgBack")?.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(tappedOnBack(_:)))
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.isTranslucent = false
        //self.navigationController?.navigationBar.barTintColor = Color.COLOR_NAVIGATION_BACKGROUND
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        //self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
    }
    
    
    // MARK: - Button Tapped Event
    @IBAction func tappedOnBack(_ sender:UIBarButtonItem)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func tappedOnNext(_ sender:UIButton)
    {
        if imgProdCoverImage != nil {
            let addProdVC = self.storyboard?.instantiateViewController(withIdentifier: "AddProductrVC2") as! AddProductrVC2
            addProdVC.imgProdCoverImage = self.imgProdCoverImage
            self.navigationController?.pushViewController(addProdVC, animated: true)
        }else{
            appDelegate.showAlertMessage(strMessage: "Please select product image")
        }
        
    }
    
    @IBAction func tappedOnProfile(_ sender:UIControl){
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let action1 = UIAlertAction(title: "Open Camera", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            _ = PresentPhotoCamera(target: self, canEdit: false)
        })
        let action2 = UIAlertAction(title: "Photo Library", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            _ = PresentPhotoLibrary(target: self, canEdit: false)
        })
        let action3 = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alert.addAction(action1)
        alert.addAction(action2)
        alert.addAction(action3)
        present(alert, animated: true) {
            
        }
    }
    
    //MARK: - UIImagePickerControllerDelegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        imgProdCoverImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        imageViewProdCover.image = imgProdCoverImage
        dismiss(animated: true, completion: nil)
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
