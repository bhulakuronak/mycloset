//
//  AddProductrVC2.swift
//  MyCloset
//
//  Created by M on 14/03/18.
//  Copyright © 2018 YDSHaridham. All rights reserved.
//

import UIKit
import Alamofire
class AddProductrVC2: UIViewController, UITextFieldDelegate, UITextViewDelegate, SelectBrandProtocol {
   
    

    @IBOutlet var txtItemName:UITextField!
    @IBOutlet var txtProdDescription:UITextView!
    @IBOutlet var txtPrice:UITextField!
    @IBOutlet var txtType:UITextField!
    @IBOutlet var txtCategories:UITextField!
    @IBOutlet var txtGender:UITextField!
    @IBOutlet var txtBrand:UITextField!
    var strBrandId:String = ""
    @IBOutlet var txtCondition:UITextField!
    @IBOutlet var txtSize:UITextField!
    @IBOutlet var viewSize:UIView!
    @IBOutlet var heightViewSize:NSLayoutConstraint!
    
    @IBOutlet var btnBuyNow:UIButton!
    @IBOutlet var btnAuction:UIButton!
    var strTrade:String = ""
    
    @IBOutlet var scrollView:UIScrollView!
    
    var strTextFieldSelected:String = ""
    
    let chooseDropDown = DropDown()
    let arrGender = ["Man", "Woman"]
    let arrCategories = ["READY-TO-WEAR", "SHOES", "ACCESSORIES"]
    
    var imgProdCoverImage:UIImage!
    
    //MANS
    let arrMReadyWearType = ["Knitwear", "Jackets & Coats", "Leather & Casual Jackets", "Suits", "Shirts", "Sweatshirts", "T-shirts & Polos", "Trousers & Shorts", "Denim", "Activewear"]
    let arrMShoesType = ["Moccasins & Loafers", "Lace Ups", "Boots", "Slippers", "Drivers", "Sandals & Thongs", "Sneakers", "Precious Skins"]
    let arrMAccessoriesType = ["Wallets & Small Accessories", "Belts", "Ties", "Scarves", "Hats & Gloves", "Watches", "Jewellery", "Eyewear", "Socks"]
    let arrMShoesSize = ["40", "41", "41.5", "42", "43", "43.5", "44", "44.5", "45", "45.5", "46", "47.5", "48", "50"]
    
    //WOMAN
    let arrWReadyWearType = ["Dresses","Jackets","Coats & Furs","Leather & Casual Jackets","Tops & Shirts","Sweaters & Cardigans","Sweatshirts & T-Shirts","Skirts","Pants & Shorts","Denim","Activewear"]
    let arrWShoesType = ["Pumps", "Sandals","Moccasins & Loafers","Ballerinas","Slippers & Mules","Boots & Booties","Espadrilles & Wedges", "Sneakers"]
    let arrWAccessoriesType = ["Luggage & Lifestyle Bags","Wallets & Small Accessories","Belts","Silks & Scarves","Hats & Gloves", "Jewellery","Watches","Eyewear","Socks & Tights","Runway Accessories"]
    let arrWShoesSize = ["35.5", "36", "37", "37.5", "38", "38.5", "39", "39.5", "40", "41", "41.5", "42", "43"]
    


    
    let arrCondition = ["Never used", "As good as new", "Fine but used", "Worn"]
    let arrSize = ["S", "M", "L", "XS", "XL", "XXL"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //------Navigation Button------
        self.title = "Account"
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "imgBack")?.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(tappedOnBack(_:)))
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isTranslucent = false
        //self.navigationController?.navigationBar.barTintColor = Color.COLOR_NAVIGATION_BACKGROUND
        let description = txtProdDescription.text.trimmingCharacters(in: .whitespacesAndNewlines)
        if description == "Product Description" {
            self.txtProdDescription.textColor = Color.placeHolderColor
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        //self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupDropDown(textField:UITextField) {
        chooseDropDown.anchorView = textField
        chooseDropDown.dismissMode = .onTap
        chooseDropDown.direction = .any
        chooseDropDown.bottomOffset = CGPoint(x: 0, y: textField.bounds.height)
        
        // You can also use localizationKeysDataSource instead. Check the docs.
        if strTextFieldSelected == "txtGender" {
            chooseDropDown.dataSource = arrGender
        } else if strTextFieldSelected == "txtCategories"  {
            chooseDropDown.dataSource = arrCategories
        }else if self.strTextFieldSelected == "txtType"  {
            if txtGender.text == "Man"  {
                if txtCategories.text == "READY-TO-WEAR" {
                    chooseDropDown.dataSource = arrMReadyWearType
                }else if txtCategories.text == "SHOES" {
                    chooseDropDown.dataSource = arrMShoesType
                }else if txtCategories.text == "ACCESSORIES" {
                    chooseDropDown.dataSource = arrMAccessoriesType
                }
            }else if txtGender.text == "Woman" {
                if txtCategories.text == "READY-TO-WEAR" {
                    chooseDropDown.dataSource = arrWReadyWearType
                }else if txtCategories.text == "SHOES" {
                    chooseDropDown.dataSource = arrWShoesType
                }else if txtCategories.text == "ACCESSORIES" {
                    chooseDropDown.dataSource = arrWAccessoriesType
                }
            }
        }else if strTextFieldSelected == "txtCondition" {
            chooseDropDown.dataSource = arrCondition
        }else if strTextFieldSelected == "txtSize" {
            if txtGender.text == "Man"  {
                if txtCategories.text == "READY-TO-WEAR" {
                    chooseDropDown.dataSource = arrSize
                }else if txtCategories.text == "SHOES" {
                    chooseDropDown.dataSource = arrMShoesSize
                }
            }else if txtGender.text == "Woman" {
                if txtCategories.text == "READY-TO-WEAR" {
                    chooseDropDown.dataSource = arrSize
                }else if txtCategories.text == "SHOES" {
                    chooseDropDown.dataSource = arrWShoesSize
                }
            }
        }
        
        
        // Action triggered on selection
        chooseDropDown.selectionAction = { [unowned self] (index, item) in
           /* if self.strTextFieldSelected == "txtType"  {
                self.txtType.text = self.arrType[index]
            }else if self.strTextFieldSelected == "txtGender" {
                self.txtGender.text = self.arrGender[index]
            }else if self.strTextFieldSelected == "txtCondition" {
                self.txtCondition.text = self.arrCondition[index]
            }else if self.strTextFieldSelected == "txtSize" {
                self.txtSize.text = self.arrSize[index]
            }*/
            
            if self.strTextFieldSelected == "txtGender" {
                self.txtGender.text = self.arrGender[index]
                self.txtCategories.text = ""
                self.txtType.text = ""
                self.txtBrand.text = ""
                self.txtSize.text = ""
            } else if self.strTextFieldSelected == "txtCategories"  {
                self.txtCategories.text = self.arrCategories[index]
                self.txtType.text = ""
                self.txtSize.text = ""
                if self.txtCategories.text == "ACCESSORIES" {
                    self.viewSize.isHidden = true
                    self.txtSize.text = ""
                    self.heightViewSize.constant = 0
                    UIView.animate(withDuration: 0.5) {
                        self.view.layoutIfNeeded()
                    }
                }else{
                    self.viewSize.isHidden = false
                    self.heightViewSize.constant = 50
                    UIView.animate(withDuration: 0.5) {
                        self.view.layoutIfNeeded()
                    }
                }
            }else if self.self.strTextFieldSelected == "txtType"  {
                if self.txtGender.text == "Man"  {
                    if self.txtCategories.text == "READY-TO-WEAR" {
                        self.txtType.text = self.arrMReadyWearType[index]
                    }else if self.txtCategories.text == "SHOES" {
                        self.txtType.text = self.arrMShoesType[index]
                    }else if self.txtCategories.text == "ACCESSORIES" {
                        self.txtType.text = self.arrMAccessoriesType[index]
                    }
                }else if self.txtGender.text == "Woman" {
                    if self.txtCategories.text == "READY-TO-WEAR" {
                        self.txtType.text = self.arrWReadyWearType[index]
                    }else if self.txtCategories.text == "SHOES" {
                        self.txtType.text = self.arrWShoesType[index]
                    }else if self.txtCategories.text == "ACCESSORIES" {
                        self.txtType.text = self.arrWAccessoriesType[index]
                    }
                }
            }else if self.strTextFieldSelected == "txtCondition" {
                self.txtCondition.text = self.arrCondition[index]
            }else if self.strTextFieldSelected == "txtSize" {
                if self.txtGender.text == "Man"  {
                    if self.txtCategories.text == "READY-TO-WEAR" {
                        self.txtSize.text = self.arrSize[index]
                    }else if self.txtCategories.text == "SHOES" {
                        self.txtSize.text = self.arrMShoesSize[index]
                    }
                }else if self.txtGender.text == "Woman" {
                    if self.txtCategories.text == "READY-TO-WEAR" {
                        self.txtSize.text = self.arrSize[index]
                    }else if self.txtCategories.text == "SHOES" {
                        self.txtSize.text = self.arrWShoesSize[index]
                    }
                }
            }
        }
        chooseDropDown.show()
    }
    
    func selectBrand(brandName: String, brandId:String) {
        txtBrand.text = brandName
        strBrandId = brandId
    }
    
    func uploadProduct()  {
        
        
        let userDetail = getMyUserDefaults(key: MyUserDefaults.UserData) as! [String:Any]
        if userDetail == nil {
            appDelegate.showAlertMessage(strMessage: "Please login first.")
            return
        }
        var parameters = [String : Any]()
        parameters["i_key"] = WebURL.appKey
        parameters["product_name"] = txtItemName.text ?? ""
        parameters["product_description"] = txtProdDescription.text
        parameters["product_category"] = strBrandId
        parameters["product_status"] = txtCondition.text ?? ""
        parameters["product_price"] = txtPrice.text ?? ""
        parameters["product_isFixed"] = ""
        parameters["user_id"] = userDetail["user_id"] ?? ""
        parameters["type_of_cloths"] = txtCategories.text ?? ""
        parameters["type_of_gender"] = txtGender.text ?? ""
        parameters["product_size"] = txtSize.text ?? ""
        parameters["auction"] = strTrade
        parameters["subcat"] = txtType.text ?? ""
        appDelegate.showHud()
            
            
        upload(multipartFormData: { (multipartFormData) in
            if self.imgProdCoverImage != nil {
                    
                let data1 = UIImageJPEGRepresentation(self.imgProdCoverImage, 0.7)
                    multipartFormData.append(data1!, withName: "product_cover_picture", fileName: "Product\(arc4random_uniform(3) + 1).jpg", mimeType: "image/jpg")
                }
                
                
                for (key, value) in parameters {
                    multipartFormData.append((value as! String).data(using: .utf8)!, withName: key)
                }
                
            }, to: WebURL.addProduct, method: .post, encodingCompletion: { (encodingResult) in
                switch encodingResult {
                case .success(let upload, _, _):
                    
                    
                    upload.validate()
                    upload.responseJSON { response in
                        
                        appDelegate.hideHud()
                        if response.result.error != nil {
                            print("failure")
                            UIAlertView(title: "Fail", message: "Please retry again.", delegate: nil, cancelButtonTitle: "Ok").show()
                        } else {
                            print("success")
                            print("addProduct Result:\(response)")
                            if let dictResult:[String:Any] = response.result.value as! [String : Any]? {
                                if (dictResult["o_returnCode"] as! Bool){
                                    let successMsg = dictResult["o_message"] as! String
                                    showAlert(msg: successMsg, completion: { (true) in
                                        self.navigationController?.popToRootViewController(animated: true)
                                    })
                                }else{
                                    let errorMsg = dictResult["o_message"] as! String
                                    appDelegate.showAlertMessage(strMessage: errorMsg)
                                }
                            }
                        }
                    }
                    
                case .failure(let encodingError):
                    print(encodingError)
                    showAlert(msg: "This application requires internet connection", completion: nil)
                    //failure
                }
            })
        
    }
    
    // MARK: - Button Tapped Event
    @IBAction func tappedOnBack(_ sender:UIBarButtonItem)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func tappedOnTradeButton(_ sender:UIButton)
    {
        if btnBuyNow == sender {
            self.btnBuyNow.isSelected = true
            self.btnAuction.isSelected = false
            strTrade = "0"
        }else if btnAuction == sender {
            self.btnBuyNow.isSelected = false
            self.btnAuction.isSelected = true
            strTrade = "1"
        }
    }
    
    @IBAction func tappedOnThrow(_ sender:UIButton)
    {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func tappedOnHangUp(_ sender:UIButton) {
        let strName = txtItemName.text
        let strProdDescription = txtProdDescription.text
        let strPrice = txtPrice.text
        let strGender = txtGender.text
        let strCategory = txtCategories.text
        let strType = txtType.text
        let strBrand = txtBrand.text
        let strCondition = txtCondition.text
        let strSize = txtSize.text
        if strName == "" {
            appDelegate.showAlertMessage(strMessage: "Please enter product name.")
        }else if strProdDescription == "Product Description" {
            appDelegate.showAlertMessage(strMessage: "Please enter product description.")
        }else if strPrice == "" {
            appDelegate.showAlertMessage(strMessage: "Please enter price.")
        }else if strGender == "" {
            appDelegate.showAlertMessage(strMessage: "Please select gender.")
        }else if strCategory == "" {
            appDelegate.showAlertMessage(strMessage: "Please select category.")
        }else if strType == "" {
            appDelegate.showAlertMessage(strMessage: "Please select product type.")
        }else if strBrand == "" {
            appDelegate.showAlertMessage(strMessage: "Please select product brand.")
        }else if strCondition == "" {
            appDelegate.showAlertMessage(strMessage: "Please select product condition.")
        }else if strSize == "" && strCategory != "ACCESSORIES" {
            appDelegate.showAlertMessage(strMessage: "Please select product size.")
        }else if strTrade == "" {
            appDelegate.showAlertMessage(strMessage: "Please select product trade type.")
        }else {
            uploadProduct()
        }
    }
    
    //MARK: - UITextFieldDelegate
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtItemName  {
            strTextFieldSelected = "txtItemName"
            textField.inputAccessoryView = self.toolbarInit()
        }else if textField == txtPrice  {
            strTextFieldSelected = "txtPrice"
            textField.inputAccessoryView = self.toolbarInit()
        }else if textField == txtGender  {
            strTextFieldSelected = "txtGender"
            setupDropDown(textField: txtGender)
            return false
        }else if textField == txtCategories  {
            strTextFieldSelected = "txtCategories"
            setupDropDown(textField: txtCategories)
            return false
        }else if textField == txtType  {
            strTextFieldSelected = "txtType"
            if txtGender.text == "" {
                appDelegate.showAlertMessage(strMessage: "Please select gender")
            }else if txtCategories.text == "" {
                appDelegate.showAlertMessage(strMessage: "Please select categories")
            }else{
                setupDropDown(textField: txtType)
            }
            return false
        }else if textField == txtBrand  {
            strTextFieldSelected = "txtBrand"
            //setupDropDown(textField: txtBrand)
            let brandView = self.storyboard?.instantiateViewController(withIdentifier: "BrandViewController") as! BrandViewController
            brandView.delegate = self
            self.navigationController?.pushViewController(brandView, animated: true)
            return false
        }else if textField == txtCondition  {
            strTextFieldSelected = "txtCondition"
            setupDropDown(textField: txtCondition)
            return false
        }else if textField == txtSize  {
            strTextFieldSelected = "txtSize"
            if txtGender.text == "" {
                appDelegate.showAlertMessage(strMessage: "Please select gender")
            }else if txtCategories.text == "" {
                appDelegate.showAlertMessage(strMessage: "Please select categories")
            }else{
                setupDropDown(textField: txtSize)
            }
            
            return false
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    //MARK: - UITextViewDelegate
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        strTextFieldSelected = "txtProdDescription"
        textView.inputAccessoryView = self.toolbarInit()
        let description = txtProdDescription.text.trimmingCharacters(in: .whitespacesAndNewlines)
        if description == "Product Description" {
            self.txtProdDescription.textColor = UIColor.black
            self.txtProdDescription.text = ""
        }
        return true
    }
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        let description = txtProdDescription.text.trimmingCharacters(in: .whitespacesAndNewlines)
        if description == "" {
            self.txtProdDescription.textColor = Color.placeHolderColor
            self.txtProdDescription.text = "Product Description"
        }
        return true
    }
    
    
    
    //MARK: - NotificationCenter Register
    func registerNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: .UIKeyboardWillHide, object: nil)
    }
    
    func unregisterNotifications() {
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillHide, object: nil)
    }
    
    @objc func keyboardWillShow(notification: NSNotification){
        guard let keyboardFrame = notification.userInfo![UIKeyboardFrameBeginUserInfoKey] as? NSValue else { return }
        scrollView.contentInset.bottom = view.convert(keyboardFrame.cgRectValue, from: nil).size.height + 20
    }
    
    @objc func keyboardWillHide(notification: NSNotification){
        scrollView.contentInset.bottom = 0
    }
    
    // MARK: - Keyboard
    func toolbarInit() -> UIToolbar
    {
        let toolBar = UIToolbar()
        toolBar.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 40)
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.barTintColor = Color.keyboardHeaderColor
        toolBar.tintColor = UIColor.white
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(resignKeyboard))
        let previousButton:UIBarButtonItem! = UIBarButtonItem()
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        
        previousButton.customView = self.prevNextSegment()
        toolBar.setItems([previousButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        toolBar.sizeToFit()
        
        return toolBar;
    }
    
    func prevNextSegment() -> UISegmentedControl
    {
        let prevNextSegment = UISegmentedControl()
        prevNextSegment.isMomentary = true
        prevNextSegment.tintColor = UIColor.white
        let barbuttonFont = UIFont(name: FontName.OpenSansRegular, size: 15) ?? UIFont.systemFont(ofSize: 15)
        prevNextSegment.setTitleTextAttributes([NSAttributedStringKey.font: barbuttonFont, NSAttributedStringKey.foregroundColor:UIColor.clear], for: UIControlState.disabled)
        if(DeviceType.IS_IPHONE_6PLUS)
        {
            prevNextSegment.frame = CGRect(x: 0, y: 0, width: 130, height: 28) //CGRectMake(0,0,130, 28)
        }
        else
        {
            prevNextSegment.frame = CGRect(x: 0, y: 0, width: 130, height: 40) //CGRectMake(0,0,130, 40)
        }
        prevNextSegment.insertSegment(withTitle: "Previous", at: 0, animated: false)
        prevNextSegment.insertSegment(withTitle: "Next", at: 1, animated: false)
        
        prevNextSegment.addTarget(self, action: #selector(prevOrNext), for: UIControlEvents.valueChanged)
        return prevNextSegment;
    }
    
    @objc func prevOrNext(segm: UISegmentedControl)
    {
        if (segm.selectedSegmentIndex == 1)
        {
            if strTextFieldSelected == "txtItemName" {
                txtProdDescription.becomeFirstResponder()
            }else if strTextFieldSelected == "txtProdDescription" {
                txtPrice.becomeFirstResponder()
            }else if strTextFieldSelected == "txtPrice"{
                txtPrice.resignFirstResponder()
            }
        }else
        {
            if strTextFieldSelected == "txtItemName" {
                txtItemName.resignFirstResponder()
            }else if strTextFieldSelected == "txtProdDescription" {
                txtItemName.becomeFirstResponder()
            }else if strTextFieldSelected == "txtPrice"{
                txtProdDescription.becomeFirstResponder()
            }
        }
    }
    
    @objc func resignKeyboard()
    {
        self.view.endEditing(true)
        
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        resignKeyboard()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
