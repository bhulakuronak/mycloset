//
//  WelcomeViewController.swift
//  MyCloset
//
//  Created by ronak patel on 12/03/18.
//  Copyright © 2018 YDSHaridham. All rights reserved.
//

import UIKit
import Alamofire
import FirebaseAuth

class LoginViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet var txtEmail:UITextField!
    @IBOutlet var txtPassword:UITextField!
    @IBOutlet var scrlView:UIScrollView!
    
    var strTextFieldSelected:String = ""
    
    //Forgot Password
    @IBOutlet var scrollView:UIScrollView!
    @IBOutlet var forgotView:UIView!
    @IBOutlet var forgotContainerView:UIView!
    @IBOutlet var newPasswordContainerView:UIView!
    @IBOutlet var txtEmailForgot:UITextField!
    
    @IBOutlet var txtOTP:UITextField!
    @IBOutlet var txtNewPassword:UITextField!
    @IBOutlet var txtCPassword:UITextField!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Sign in with email"
        self.navigationController?.isNavigationBarHidden = false
        //self.navigationController?.navigationBar.tintColor = Color.COLOR_GREEN;
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white];
        
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        
        //------Navigation Button------
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "imgBack1")?.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(tappedOnBack(_:)))
        
        
        self.hideKeyboardWhenTappedAround()
        
        scrollView.isHidden = true

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        registerNotifications()
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        unregisterNotifications()
    }
    
    // MARK: - Button Tapped Event
    @IBAction func tappedOnBack(_ sender:UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Button Tapped Event
    @IBAction func tappedOnLogin(_ sender:UIButton) {
        if !((txtEmail.text?.isValidEmail())!) {
            appDelegate.showAlertMessage(strMessage: "Please enter valid email id")
        }else if txtPassword.text?.length == 0  {
            appDelegate.showAlertMessage(strMessage: "Please enter password")
        }else{
            
            var parameters = ["i_key":WebURL.appKey,
                              "email":txtEmail.text ?? "",
                              "password":txtPassword.text ?? ""] as [String : Any]
            
            if let deviceToken = getMyUserDefaults(key: MyUserDefaults.FCMDeviceToken) as? String {
                parameters["device_type"] = "ios"
                parameters["device_id"] = deviceToken
            }
            appDelegate.showHud()
            request(WebURL.login, method: .post, parameters: parameters).responseJSON(completionHandler: { (response) in
                print("Login Result:\(response)")
                appDelegate.hideHud()
                if response.result.isSuccess {
                    if let dictResult:[String:Any] = response.result.value as! [String : Any]? {
                        if (dictResult["o_returnCode"] as! Bool){
                            setMyUserDefaults(value: true, key: MyUserDefaults.isLogin)
                            setMyUserDefaults(value: dictResult["o_data"] as! [String:Any], key: MyUserDefaults.UserData)
                            appDelegate.updateDeviceToken(strToken: getMyUserDefaults(key: MyUserDefaults.FCMDeviceToken) as! String)
                            
                            let userDetail = getMyUserDefaults(key: MyUserDefaults.UserData) as! [String:Any]
                            if let userEmail = userDetail["email"] as? String {
                                FIRAuth.auth()?.createUser(withEmail: userEmail, password: "123456789", completion: { (user, error) in
                                    print("createUser:\(String(describing: user))")
                                    print("Error:\(String(describing: error?.localizedDescription))")
                                    if error != nil {
                                        FIRAuth.auth()?.signIn(withEmail: userEmail, password: "123456789", completion: { (user, error) in
                                            print("Login:\(String(describing: user))")
                                            print("Error:\(String(describing: error))")
                                        })
                                    }
                                })
//
                            }
                            
                            let mainViewController = self.storyboard?.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
                            let leftViewController = self.storyboard?.instantiateViewController(withIdentifier: "LeftViewController") as! LeftViewController
                            let nvc: NavigationController = NavigationController(rootViewController: mainViewController)
                            //UINavigationBar.appearance().tintColor = UIColor(hex: "689F38")
                            
                            leftViewController.mainViewController = nvc
                            
                            
                            let slideMenuController = ExSlideMenuController(mainViewController: nvc, leftMenuViewController: leftViewController)
                            slideMenuController.automaticallyAdjustsScrollViewInsets = true
                            //slideMenuController.delegate = mainViewController as SlideMenuControllerDelegate
                            //self.window?.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
                            appDelegate.window?.rootViewController = slideMenuController
                            
                            //self.presentingViewController?.dismiss(animated: true, completion: nil)
                        }else{
                            setMyUserDefaults(value: false, key: MyUserDefaults.isLogin)
                            let errorMsg = dictResult["o_message"] as! String
                            appDelegate.showAlertMessage(strMessage: errorMsg)
                        }
                    }
                }
            })
        }
    }
    
    @IBAction func tappedOnForgotPassword(_ sender:UIButton){
        
        scrollView.isHidden = false
        forgotContainerView.isHidden = false
        forgotContainerView.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {() -> Void in
            self.forgotContainerView.transform = .identity
        }, completion: {(finished: Bool) -> Void in
            // do something once the animation finishes, put it here
        })
    }
    
    @IBAction func tappedOnForgotCancel(_ sender:UIButton){
        scrollView.isHidden = true
    }
    
    @IBAction func tappedOnForgotSubmit(_ sender:UIButton) {
        let strEmail = txtEmailForgot.text
        if strEmail == ""{
            appDelegate.showAlertMessage(strMessage: "Please enter your email id")
        }else if !(strEmail!.isValidEmail()) {
            appDelegate.showAlertMessage(strMessage: "Please enter valid email id")
        }else{
            let parameters = ["i_key":WebURL.appKey,
                              "email":txtEmailForgot.text ?? ""] as [String : Any]
            appDelegate.showHud()
            request(WebURL.forgotPassword, method: .post, parameters: parameters).responseJSON(completionHandler: { (response) in
                print("forgotPassword Result:\(response)")
                appDelegate.hideHud()
                if response.result.isSuccess {
                    if let dictResult:[String:Any] = response.result.value as! [String : Any]? {
                        if (dictResult["o_returnCode"] as! Bool){
                            self.forgotContainerView.isHidden = true
                            self.newPasswordContainerView.isHidden = false
                            self.newPasswordContainerView.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
                            UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {() -> Void in
                                self.newPasswordContainerView.transform = .identity
                            }, completion: {(finished: Bool) -> Void in
                                // do something once the animation finishes, put it here
                            })
                        }else{
                            
                            let errorMsg = dictResult["o_message"] as! String
                            appDelegate.showAlertMessage(strMessage: errorMsg)
                        }
                        
                    }
                }
            })
        }
    }
    
    @IBAction func tappedOnChangePassword(_ sender:UIButton) {
        
        if txtOTP.text == "" {
            appDelegate.showAlertMessage(strMessage: "Please enter OTP")
        }else if (txtNewPassword.text?.length)! < 8 {
            appDelegate.showAlertMessage(strMessage: "Password must be > 8 characters")
        }else if txtNewPassword.text != txtCPassword.text {
            appDelegate.showAlertMessage(strMessage: "Password id do not match")
        }else{
            let parameters = ["i_key":WebURL.appKey,
                              "email":txtEmailForgot.text ?? "",
                              "resetpassword_token":txtOTP.text ?? "",
                              "newpassword":txtNewPassword.text ?? ""] as [String : Any]
            appDelegate.showHud()
            request(WebURL.resetPassword, method: .post, parameters: parameters).responseJSON(completionHandler: { (response) in
                print("resetPassword Result:\(response)")
                appDelegate.hideHud()
                if response.result.isSuccess {
                    if let dictResult:[String:Any] = response.result.value as! [String : Any]? {
                        if (dictResult["o_returnCode"] as! Bool){
                            self.scrollView.isHidden = true
                        }else{
                            
                            let errorMsg = dictResult["o_message"] as! String
                            appDelegate.showAlertMessage(strMessage: errorMsg)
                        }
                        
                    }
                }
            })
        }
    }
    
    //MARK: - UITextFieldDelegate
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtEmail  {
            strTextFieldSelected = "txtEmail"
            textField.inputAccessoryView = self.toolbarInit()
        }else if textField == txtPassword  {
            strTextFieldSelected = "txtPassword"
            textField.inputAccessoryView = self.toolbarInit()
        }else if textField == txtOTP  {
            strTextFieldSelected = "txtOTP"
            textField.inputAccessoryView = self.toolbarInit()
        }else if textField == txtNewPassword  {
            strTextFieldSelected = "txtNewPassword"
            textField.inputAccessoryView = self.toolbarInit()
        }else if textField == txtCPassword  {
            strTextFieldSelected = "txtCPassword"
            textField.inputAccessoryView = self.toolbarInit()
        }
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == txtEmail {
            txtPassword.becomeFirstResponder()
        }else if textField == txtPassword {
            txtPassword.resignFirstResponder()
        }else{
            textField.resignFirstResponder()
        }
        return true
    }
    
    
    //MARK: - NotificationCenter Register
    func registerNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: .UIKeyboardWillHide, object: nil)
    }
    
    func unregisterNotifications() {
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillHide, object: nil)
    }
    
    @objc func keyboardWillShow(notification: NSNotification){
        guard let keyboardFrame = notification.userInfo![UIKeyboardFrameBeginUserInfoKey] as? NSValue else { return }
        scrlView.contentInset.bottom = view.convert(keyboardFrame.cgRectValue, from: nil).size.height + 20
        scrollView.contentInset.bottom = view.convert(keyboardFrame.cgRectValue, from: nil).size.height + 20
    }
    
    @objc func keyboardWillHide(notification: NSNotification){
        scrollView.contentInset.bottom = 0
        scrlView.contentInset.bottom = 0
    }
    
    // MARK: - Keyboard
    func toolbarInit() -> UIToolbar
    {
        let toolBar = UIToolbar()
        toolBar.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 40)
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.barTintColor = Color.keyboardHeaderColor
        toolBar.tintColor = UIColor.white
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(resignKeyboard))
        let previousButton:UIBarButtonItem! = UIBarButtonItem()
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        
        previousButton.customView = self.prevNextSegment()
        toolBar.setItems([previousButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        toolBar.sizeToFit()
        
        return toolBar;
    }
    
    func prevNextSegment() -> UISegmentedControl
    {
        let prevNextSegment = UISegmentedControl()
        prevNextSegment.isMomentary = true
        prevNextSegment.tintColor = UIColor.white
        let barbuttonFont = UIFont(name: FontName.OpenSansRegular, size: 15) ?? UIFont.systemFont(ofSize: 15)
        prevNextSegment.setTitleTextAttributes([NSAttributedStringKey.font: barbuttonFont, NSAttributedStringKey.foregroundColor:UIColor.clear], for: UIControlState.disabled)
        if(DeviceType.IS_IPHONE_6PLUS)
        {
            prevNextSegment.frame = CGRect(x: 0, y: 0, width: 130, height: 28) //CGRectMake(0,0,130, 28)
        }
        else
        {
            prevNextSegment.frame = CGRect(x: 0, y: 0, width: 130, height: 40) //CGRectMake(0,0,130, 40)
        }
        prevNextSegment.insertSegment(withTitle: "Previous", at: 0, animated: false)
        prevNextSegment.insertSegment(withTitle: "Next", at: 1, animated: false)
        
        prevNextSegment.addTarget(self, action: #selector(prevOrNext), for: UIControlEvents.valueChanged)
        return prevNextSegment;
    }
    
    @objc func prevOrNext(segm: UISegmentedControl)
    {
        if (segm.selectedSegmentIndex == 1)
        {
            if strTextFieldSelected == "txtEmail" {
                txtPassword.becomeFirstResponder()
            }else if strTextFieldSelected == "txtPassword" {
                txtPassword.resignFirstResponder()
            }else if strTextFieldSelected == "txtOTP"{
                txtNewPassword.becomeFirstResponder()
            }else if strTextFieldSelected == "txtNewPassword"{
                txtCPassword.becomeFirstResponder()
            }else if strTextFieldSelected == "txtCPassword"{
                txtCPassword.resignFirstResponder()
            }
        }else
        {
            if strTextFieldSelected == "txtEmail" {
                txtEmail.resignFirstResponder()
            }else if strTextFieldSelected == "txtPassword" {
                txtEmail.becomeFirstResponder()
            }else if strTextFieldSelected == "txtOTP"{
                txtOTP.resignFirstResponder()
            }else if strTextFieldSelected == "txtNewPassword"{
                txtOTP.becomeFirstResponder()
            }else if strTextFieldSelected == "txtCPassword"{
                txtNewPassword.becomeFirstResponder()
            }
        }
    }
    
    @objc func resignKeyboard()
    {
        self.view.endEditing(true)
        
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        resignKeyboard()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
