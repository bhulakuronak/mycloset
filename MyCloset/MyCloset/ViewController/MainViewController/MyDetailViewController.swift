//
//  MyDetailViewController.swift
//  MyCloset
//
//  Created by M on 19/03/18.
//  Copyright © 2018 YDSHaridham. All rights reserved.
//

import UIKit
import Alamofire
class WardrodeCell: UICollectionViewCell {
    @IBOutlet var imgViewProduct:UIImageView!
}

class BuyingCell: UITableViewCell {
    @IBOutlet var imgViewProduct:UIImageView!
    @IBOutlet var lblProdName:UILabel!
    @IBOutlet var btnPay:UIButton!
    @IBOutlet var btnMessage:UIButton!
}

class MyDetailViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout, UITableViewDelegate, UITableViewDataSource {
    
    
    @IBOutlet var btnMyWardrode:UIButton!
    @IBOutlet var btnBuying:UIButton!
    @IBOutlet var btnAccount:UIButton!
    
    @IBOutlet var lblName:UILabel!
    @IBOutlet var lblEmail:UILabel!
    
    @IBOutlet var collectionWardrode:UICollectionView!
    var arrWardrodeProduct = [Any]()
    var arrBuyingProduct = [Any]()
    
    var arr = [Any]()
    
    @IBOutlet var tblBuying:UITableView!

    @IBOutlet var leadingConstraint:NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "imgBack")?.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(tappedOnBack(_:)))
        tblBuying.estimatedRowHeight = 80
        
        self.getInterstedProductListing()
        
        collectionWardrode.isHidden = false
        tblBuying.isHidden = true
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Button Tapped Event
    @IBAction func tappedOnBack(_ sender:UIBarButtonItem)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func tappedOnTabBar(_ sender:UIButton) {
        if btnMyWardrode == sender {
            self.leadingConstraint.constant = 0
            UIView.animate(withDuration: 0.8) {
                self.view.layoutIfNeeded()
            }
            collectionWardrode.isHidden = false
            tblBuying.isHidden = true
        }else if btnBuying == sender {
            self.leadingConstraint.constant = ScreenSize.SCREEN_WIDTH / 3.0
            UIView.animate(withDuration: 0.8) {
                self.view.layoutIfNeeded()
            }
            collectionWardrode.isHidden = true
            tblBuying.isHidden = false
        }else if btnAccount == sender {
            self.leadingConstraint.constant = (ScreenSize.SCREEN_WIDTH / 3.0) * 2
            UIView.animate(withDuration: 0.8) {
                self.view.layoutIfNeeded()
            }
            collectionWardrode.isHidden = true
            tblBuying.isHidden = true
        }
    }
    
    @IBAction func tappedOnPay(_ sender:UIButton) {
        let dictProd = self.arrBuyingProduct[sender.tag] as! [String:Any]
        let paymentView = storyBoards.Main.instantiateViewController(withIdentifier: "PaymentViewController") as! PaymentViewController
        paymentView.productDetail = dictProd
        self.navigationController?.pushViewController(paymentView, animated: true)
    }
    
    
    func getInterstedProductListing() {
        
        let userDetail = getMyUserDefaults(key: MyUserDefaults.UserData) as! [String:Any]
        if userDetail == nil {
            appDelegate.showAlertMessage(strMessage: "Please login first.")
            return
        }
        self.lblName.text = userDetail["name"] as? String
        self.lblEmail.text = "\(userDetail["phonenumber"] as! String)|\(userDetail["email"] as! String)"
        
        let parameters = ["i_key":WebURL.appKey,
                          "user_id":userDetail["user_id"] ?? ""] as [String : Any]
        appDelegate.showHud()
        request(WebURL.userInterestProduct, method: .post, parameters: parameters).responseJSON(completionHandler: { (response) in
            print("userInterestProduct List:\(response)")
            appDelegate.hideHud()
            if response.result.isSuccess {
                if let dictResult:[String:Any] = response.result.value as! [String : Any]? {
                    if (dictResult["o_returnCode"] as! Bool){
                        
                        let arrTemp = dictResult["o_data"] as! [Any]
                        let dictAll = arrTemp[0] as! [String : Any]
                        self.arrWardrodeProduct = dictAll["wardrobe"] as! [Any]
                        self.arrBuyingProduct = dictAll["buying"] as! [Any]
                        self.collectionWardrode.reloadData()
                        self.tblBuying.reloadData()
                    }else{
                        
                    }
                    
                }
            }else{
                showAlert(msg: "This application requires internet connection", completion: nil)
            }
        })
    }
    
    //MARK: - UICollectionViewDataSource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrWardrodeProduct.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "WardrodeCell", for: indexPath) as! WardrodeCell
        let dictProduct = self.arrWardrodeProduct[indexPath.row] as! [String:Any]
        let strProdUrl = dictProduct["product_cover_image"]
        cell.imgViewProduct.sd_setImage(with: URL(string: strProdUrl as! String), placeholderImage: #imageLiteral(resourceName: "imgUser"), options: .cacheMemoryOnly, completed: nil)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = ((ScreenSize.SCREEN_WIDTH - 40)/3).rounded(.towardZero)
        let height = (ScreenSize.SCREEN_WIDTH - 20)/3
        return CGSize(width: width, height: height)
    }
    
    //MARK: - UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrBuyingProduct.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BuyingCell") as! BuyingCell
        let dictProd = self.arrBuyingProduct[indexPath.row] as! [String:Any]
        cell.lblProdName.text = dictProd["product_name"] as? String
        let strProdUrl = dictProd["product_cover_image"]
        cell.imgViewProduct.sd_setImage(with: URL(string: strProdUrl as! String), placeholderImage: #imageLiteral(resourceName: "imgUser"), options: .cacheMemoryOnly, completed: nil)
        
        cell.btnPay.tag = indexPath.row
        cell.btnPay.addTarget(self, action: #selector(tappedOnPay(_:)), for: .touchUpInside)
        
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
