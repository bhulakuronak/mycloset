//
//  MainViewController.swift
//  MyCloset
//
//  Created by ronak patel on 13/03/18.
//  Copyright © 2018 YDSHaridham. All rights reserved.
//

let  MAX_BUFFER_SIZE = 3;
let  SEPERATOR_DISTANCE = 8;
let  TOPYAXIS = 75;

import UIKit
import Alamofire
import CoreLocation

class MainViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var viewTinderBackGround: UIView!
    //@IBOutlet weak var buttonUndo: UIButton!
    //@IBOutlet weak var viewActions: UIView!
    //@IBOutlet weak var viewActionHeightConstrain: NSLayoutConstraint!
    
    @IBOutlet var scrollView:UIScrollView!
    @IBOutlet var buyNowView:UIView!
    @IBOutlet var buyNowContainerView:UIView!
    @IBOutlet var txtPrice:UITextField!
    @IBOutlet var lblTitle:UILabel!
    
    var currentShowIndex = 0
    var currentIndex = 0
    var isMakeUndo = false
    var currentLostCard : TinderCard!
    var currentLoadedCardsArray = [TinderCard]()
    var allCardsArray = [TinderCard]()
    //var valueArray = ["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32","33","34","35","36"]
    
    var arrProductList = [Any]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addLeftBarMenuWithImage(#imageLiteral(resourceName: "imgMenu").withRenderingMode(.alwaysOriginal))
        self.addRightBarMenuWithImage(#imageLiteral(resourceName: "imgFilter").withRenderingMode(.alwaysOriginal))
        
        scrollView.isHidden = true
        
        let shareButton = UIButton(type: UIButtonType.custom)
        shareButton.setImage(UIImage(named: "imgShareApp"), for: .normal)
        shareButton.addTarget(self, action: #selector(tappedOnShare(_:)), for: .touchUpInside)
        self.navigationItem.titleView = shareButton;
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if UserDefaults.standard.bool(forKey: MyUserDefaults.isFilterApply) {
            self.getProductListByFilter()
        }else{
            self.getProductList()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Button Tapped Event
    @IBAction func tappedOnCamera(_ sender:UIButton) {
        _ = PresentPhotoLibrary(target: self, canEdit: false)
    }
    
    @IBAction func tappedOnShare(_ sender:UIButton) {
        let textShare = "Check out MyCloset App at: https://play.google.com/store/apps/details?id=com.myclosest.android"
        
        
        let shareAll = [textShare] as [Any]
        let activityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    @IBAction func tappedOnMyDetail(_ sender:UIButton) {
        let myDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "MyDetailViewController") as! MyDetailViewController
        self.navigationController?.pushViewController(myDetailVC, animated: true)
    }
    
    @IBAction func tappedOnMyDollor(_ sender:UIButton) {
        if currentShowIndex < allCardsArray.count {
            print("Current Index: \(currentShowIndex)")
            let dict = arrProductList[currentShowIndex] as! [String:Any]
            if dict["auction"] as! String == "0" {
                self.lblTitle.text = "Enter bid amount"
            }else{
                self.lblTitle.text = "Enter amount"
            }
            scrollView.isHidden = false
            buyNowContainerView.isHidden = false
            buyNowContainerView.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
            UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {() -> Void in
                self.buyNowContainerView.transform = .identity
            }, completion: {(finished: Bool) -> Void in
                // do something once the animation finishes, put it here
            })
        }else {
            
        }
        
        
    }
    
    @IBAction func tappedOnBuyCancel(_ sender:UIButton){
        scrollView.isHidden = true
    }
    
    @IBAction func tappedOnBuySubmit(_ sender:UIButton) {
        let strPrice = txtPrice.text
        if strPrice == "" {
            appDelegate.showAlertMessage(strMessage: "Please enter price")
            return
        }
        
        if !(strPrice?.isNumber())! {
            appDelegate.showAlertMessage(strMessage: "Please enter valid price")
            return
        }
        
        if Int(strPrice!)! < 0 {
            appDelegate.showAlertMessage(strMessage: "Please enter valid price")
            return
        }
        
        let userDetail = getMyUserDefaults(key: MyUserDefaults.UserData) as! [String:Any]
        let dictProd = arrProductList[currentShowIndex] as! [String:Any]
        let parameters = ["i_key":WebURL.appKey,
                          "user_id":userDetail["user_id"] ?? "",
                          "product_id":dictProd["product_id"] ?? "0",
                          "price":strPrice ?? "0"] as [String : Any]
        appDelegate.showHud()
        request(WebURL.addCart, method: .post, parameters: parameters).responseJSON(completionHandler: { (response) in
            print("addCart:\(response)")
            appDelegate.hideHud()
            self.scrollView.isHidden = true
            if response.result.isSuccess {
                if let dictResult:[String:Any] = response.result.value as! [String : Any]? {
                    if (dictResult["o_returnCode"] as! Bool){
                        self.sendMakeOfferMessage()
                        showAlert(msg: dictResult["o_message"] as! String, completion: { (true) in
                            //self.navigationController?.popToRootViewController(animated: true)
                        })
                        
                    }else{
                        showAlert(msg: dictResult["o_message"] as! String, completion: { (true) in
                            
                        })
                    }
                    
                }
            }
        })
    }
    
    func sendMakeOfferMessage() {
        let userDetail = getMyUserDefaults(key: MyUserDefaults.UserData) as! [String:Any]
        print("User Detail:\(userDetail)")
        let userEmail = userDetail["email"] as! String
        let userProfile = userDetail["profile_picture"] as! String
        let userFullName = userDetail["name"] as! String
        
        
        let dictProd = self.arrProductList[currentShowIndex] as! [String:Any]
        let dictUser = dictProd["userprofile"] as! [String:Any]
        let prodUserEmail = dictUser["user_email"] as! String
        let prodUserProfile = dictUser["profile_picture"] as! String
        let prodUserName = dictUser["user_name"] as! String
        
        
        
        print("MD5: \(chatId([userEmail,prodUserEmail]))")
        let groupID = chatId([userEmail,prodUserEmail])
        self.createItem(userEmail, currentUID: prodUserEmail, groupId: groupID, initials: userFullName, picture: userProfile, type: CHAT_PRIVATE)
        self.createItem(prodUserEmail, currentUID: userEmail, groupId: groupID, initials: prodUserName, picture: prodUserProfile, type: CHAT_PRIVATE)
        
        let message = "I would like to buy \(self.txtPrice.text!) at $\(dictProd["product_price"] as! String)"
        self.sendMessage(groupId: groupID, messageText: message, senderName: dictUser["user_name"] as! String, prodDeviceType: dictUser["device_type"] as! String, prodDeviceToken: dictUser["device_id"] as! String, prodUserId: dictUser["user_id"] as! String)
    }
    

    func createItem(_ userId: String, currentUID cUserId: String, groupId: String, initials: String, picture: String, type: String) {
        //----------------------------------------------------------------------------------------------
        
        let object = FObject(path: FRECENT_PATH)
        //----------------------------------------------------------------------------------------------
        let temp = "\(groupId)\(userId)"
        object[FRECENT_OBJECTID] = Checksum.md5Hash(of: temp)
        //----------------------------------------------------------------------------------------------
        object[FRECENT_USERID] = userId
        object[FRECENT_CURRENTUSERID] = cUserId
        
        object[FRECENT_GROUPID] = groupId
        
        //----------------------------------------------------------------------------------------------
        object[FRECENT_NAME] = initials
        
        object[FRECENT_PICTURE] = (picture != nil) ? picture : ""
        
        object[FRECENT_TYPE] = type
        
        //------------------------------------------------------------------------------------------------
        object[FRECENT_COUNTER] = 0
        object[FRECENT_LASTMESSAGE] = ""
        object[FRECENT_LASTMESSAGEDATE] = FIRServerValue.timestamp()
        
        //------------------------------------------------------------------------------------------------
        object.saveInBackground()
    }

    func sendMessage(groupId: String, messageText:String, senderName:String, prodDeviceType:String, prodDeviceToken:String, prodUserId:String) {
        let message = FObject(path: FMESSAGE_PATH, subpath:groupId)
        let userDetail = getMyUserDefaults(key: MyUserDefaults.UserData) as! [String:Any]
        
        
        message[FMESSAGE_GROUPID] = groupId;
        message[MESSAGE_TEXT] = messageText //"I would like to buy \(self.txtPrice.text!) at $\(dictProd["product_price"] as! String)"
        message[FMESSAGE_SENDERID] = FIRAuth.auth()?.currentUser?.uid ?? ""
        message[FMESSAGE_SENDERNAME] = senderName//dictUser["user_name"] ?? ""
        message[FMESSAGE_ISDELETED] = false;
        message[FMESSAGE_DEVICETYPE] = "ios"
        message[FMESSAGE_DEVICETOKEN] = getMyUserDefaults(key: MyUserDefaults.FCMDeviceToken)
        message[FMESSAGE_PRODUCTDEVICETYPE] = prodDeviceType
        message[FMESSAGE_PRODUCTDEVICETOKEN] = prodDeviceToken
        message[FMESSAGE_USERID] = userDetail["user_id"] ?? ""
        message[FMESSAGE_PRODUCTUSERID] = prodUserId
        
        message.saveInBackground()
        
        let dictBody:[String:String] = ["text":messageText,
                                        "username":userDetail["name"] as! String,
                                        "photo":"",
                                        "fcm_token": getMyUserDefaults(key: MyUserDefaults.FCMDeviceToken) as! String,
                                        "sound":"default"]
        print(dictBody)
        appDelegate.sendPushMessage(strToken: prodDeviceToken, deviceType: prodDeviceType, dictBody: dictBody)
    }
    
    
    //MARK: - UIImagePickerControllerDelegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        //imageProCover = info[UIImagePickerControllerOriginalImage] as! UIImage
        picker.dismiss(animated: true) {
            let addProdVC = self.storyboard?.instantiateViewController(withIdentifier: "AddProductVC") as! AddProductVC
            addProdVC.imgProdCoverImage = info[UIImagePickerControllerOriginalImage] as! UIImage
            //let navController1 = NavigationController(rootViewController: addProdVC)
            //self.slideMenuController()?.changeMainViewController(navController1, close: true)
            self.navigationController?.pushViewController(addProdVC, animated: true)
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    //MARK: - Methods
    func addLeftBarMenuWithImage(_ buttonImage: UIImage) {
        let leftButton: UIBarButtonItem = UIBarButtonItem(image: buttonImage, style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.toggleLeftMenu))
        navigationItem.leftBarButtonItem = leftButton
    }
    
    func addRightBarMenuWithImage(_ buttonImage: UIImage) {
        let rightButton: UIBarButtonItem = UIBarButtonItem(image: buttonImage, style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.toggleRightMenu))
        navigationItem.rightBarButtonItem = rightButton
    }
    
    @objc public func toggleLeftMenu() {
        slideMenuController()?.toggleLeft()
    }
    
    @objc public func toggleRightMenu() {
        print("Tapped On Filter")
        let filterVC = self.storyboard?.instantiateViewController(withIdentifier: "FilterViewController") as! FilterViewController
        self.navigationController?.pushViewController(filterVC, animated: true)
    }
    
    //MARK: - Get product list
    func getProductList() {

        var strLat = ""
        var strLong = ""
        let dictUser = getMyUserDefaults(key: MyUserDefaults.UserData) as! [String:Any]
        if let latitude = getMyUserDefaults(key: MyUserDefaults.UserLatitude) as? CLLocationDegrees {
            let longitude = getMyUserDefaults(key: MyUserDefaults.UserLongitude) as! CLLocationDegrees
            strLat = "\(latitude)"
            strLong = "\(longitude)"
        }else {
            strLat = dictUser["latitude"] as! String
            strLong = dictUser["longitude"] as! String
        }
        var cityName = ""
        if appDelegate.strCityName == "" {
            if let city = getMyUserDefaults(key: MyUserDefaults.currentCityName) as? String {
                cityName = city
            }else{
                cityName = dictUser["address"] as! String
            }
            
        }else{
            cityName = appDelegate.strCityName
        }
        let parameters = ["i_key":WebURL.appKey,
                          "address":cityName,
                          "lat":strLat,
                          "long":strLong,
                          "user_id":dictUser["user_id"]!] as [String : Any]
        print(parameters)
        appDelegate.showHud()
        request(WebURL.productList, method: .post, parameters: parameters).responseJSON(completionHandler: { (response) in
            print("Product List:\(response)")
            appDelegate.hideHud()
            if response.result.isSuccess {
                if let dictResult:[String:Any] = response.result.value as! [String : Any]? {
                    if (dictResult["o_returnCode"] as! Bool){
                        self.currentLoadedCardsArray.removeAll()
                        self.viewTinderBackGround.subviews.forEach { $0.removeFromSuperview() }
                        self.allCardsArray.removeAll()
                        self.currentIndex = 0
                        self.currentShowIndex = 0
                        self.arrProductList = dictResult["o_data"] as! [Any]
                        self.loadCards()
                    }else{
                        
                    }
                }
            }else{
                showAlert(msg: "This application requires internet connection", completion: nil)
            }
        })
    }
    
    func getProductListByFilter() {
        
        var strLat = ""
        var strLong = ""
        if let latitude = getMyUserDefaults(key: MyUserDefaults.UserLatitude) as? CLLocationDegrees {
            let longitude = getMyUserDefaults(key: MyUserDefaults.UserLongitude) as! CLLocationDegrees
            strLat = "\(latitude)"
            strLong = "\(longitude)"
        }
        
        //getMyUserDefaults(key: <#T##String#>)
        let price = getMyUserDefaults(key: MyUserDefaults.FilterPrice)
        let gender = getMyUserDefaults(key: MyUserDefaults.FilterGender)
        //let category = getMyUserDefaults(key: MyUserDefaults.FilterCategory)
        let type = getMyUserDefaults(key: MyUserDefaults.FilterType)
        let condition = getMyUserDefaults(key: MyUserDefaults.FilterCondition)
        let size = getMyUserDefaults(key: MyUserDefaults.FilterSize)
        let trade = getMyUserDefaults(key: MyUserDefaults.FilterTrade)
        let brand = getMyUserDefaults(key: MyUserDefaults.FilterBrand)
        
        var parameters = [String : Any]()
        parameters["i_key"] = WebURL.appKey
        parameters["latitude"] = strLat
        parameters["longitude"] = strLong
        parameters["posted_duration"] = ""
        
        parameters["price"] = price
        parameters["distance"] = "100"
        parameters["sort_by"] = "ACS"
        parameters["category_id"] = brand //Brand id
        parameters["type_gender"] = gender //
        parameters["auction"] = trade
        parameters["type_of_cloth"] = type
        //parameters["subcat"] = category
        parameters["size"] = size
        parameters["product_status"] = condition
        
        print("Filter Parameter: \(parameters)")
        request(WebURL.filter, method: .post, parameters: parameters).responseJSON(completionHandler: { (response) in
            print("Product List:\(response)")
            appDelegate.hideHud()
            if response.result.isSuccess {
                if let dictResult:[String:Any] = response.result.value as! [String : Any]? {
                    self.currentLoadedCardsArray.removeAll()
                    self.viewTinderBackGround.subviews.forEach { $0.removeFromSuperview() }
                    self.allCardsArray.removeAll()
                    self.currentIndex = 0
                    self.currentShowIndex = 0
                    
                    if (dictResult["o_returnCode"] as! Bool){
                        self.arrProductList = dictResult["o_data"] as! [Any]
                    }else{
                        self.arrProductList = [Any]()
                        appDelegate.showAlertMessage(strMessage: dictResult["o_message"] as! String)
                    }
                    self.loadCards()
                    
                }
            }else{
                showAlert(msg: "This application requires internet connection", completion: nil)
            }
        })
    }
    
    //MARK: - Add Cards
    func loadCards() {
        
        if self.arrProductList.count > 0 {
            let num_currentLoadedCardsArrayCap = (self.arrProductList.count > MAX_BUFFER_SIZE) ? MAX_BUFFER_SIZE : self.arrProductList.count
            for (i,_) in self.arrProductList.enumerated() {
                let newCard = createDraggableViewWithData(at: i)
                allCardsArray.append(newCard)
                if i < num_currentLoadedCardsArrayCap {
                    currentLoadedCardsArray.append(newCard)
                }
            }
            
            for (i,_) in currentLoadedCardsArray.enumerated() {
                if i > 0 {
                    viewTinderBackGround.insertSubview(currentLoadedCardsArray[i], belowSubview: currentLoadedCardsArray[i - 1])
                }
                else {
                    viewTinderBackGround.addSubview(currentLoadedCardsArray[i])
                }
                currentIndex += 1
            }
            animateCardAfterSwiping()
            self.perform(#selector(createDummyCard), with: nil, afterDelay: 1.0)
        }
    }
    
    @objc func createDummyCard() {
        
        let dummyCard = currentLoadedCardsArray.first;
        dummyCard?.shakeCard()
        UIView.animate(withDuration: 1.0, delay: 2.0, options: .curveLinear, animations: {
            //self.viewActions.alpha = 1.0
        }, completion: nil)
    }
    
    func createDraggableViewWithData(at index: Int) -> TinderCard {
        
        let card = TinderCard(frame: CGRect(x: 10, y: 0, width: viewTinderBackGround.frame.size.width - 20 , height: viewTinderBackGround.frame.size.height - 40) ,value : self.arrProductList[index] as! [String : Any])
        card.delegate = self
        card.intex = index
        return card
    }
    
    func animateCardAfterSwiping() {
        
        for (i,card) in currentLoadedCardsArray.enumerated() {
            UIView.animate(withDuration: 0.5, animations: {
                if i == 0 {
                    card.isUserInteractionEnabled = true
                }
                var frame = card.frame
                frame.origin.y = CGFloat(i * SEPERATOR_DISTANCE)
                card.frame = frame
            })
        }
    }
    
    func removeObjectAndAddNewValues() {
        
        currentLostCard = currentLoadedCardsArray.first!
        
        
        currentLoadedCardsArray.remove(at: 0)
        if currentIndex < allCardsArray.count {
            let card = allCardsArray[currentIndex]
            var frame = card.frame
            frame.origin.y = CGFloat(MAX_BUFFER_SIZE * SEPERATOR_DISTANCE)
            card.frame = frame
            currentLoadedCardsArray.append(card)
            currentIndex += 1
            viewTinderBackGround.insertSubview(currentLoadedCardsArray[MAX_BUFFER_SIZE - 1], belowSubview: currentLoadedCardsArray[MAX_BUFFER_SIZE - 2])
        }
        currentShowIndex += 1
        //print(currentIndex)
        animateCardAfterSwiping()
    }
    
    //MARK: - UITextFieldDelegate
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        textField.inputAccessoryView = self.toolbarInit()
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    // MARK: - Keyboard
    func toolbarInit() -> UIToolbar
    {
        let toolBar = UIToolbar()
        toolBar.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 40)
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.barTintColor = Color.keyboardHeaderColor
        toolBar.tintColor = UIColor.white
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(resignKeyboard))
        //let previousButton:UIBarButtonItem! = UIBarButtonItem()
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        
        //previousButton.customView = self.prevNextSegment()
        toolBar.setItems([spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        toolBar.sizeToFit()
        
        return toolBar;
    }
    
    @objc func resignKeyboard()
    {
        self.view.endEditing(true)
        
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        resignKeyboard()
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension MainViewController : TinderCardDelegate{
    
    // action called when the card goes to the left.
    func cardSwipedLeft(_ card: TinderCard) {
        removeObjectAndAddNewValues()
    }
    // action called when the card goes to the right.
    func cardSwipedRight(_ card: TinderCard) {
        removeObjectAndAddNewValues()
    }
    func updateCardView(_ card: TinderCard, withDistance distance: CGFloat) {
        //Log(@"%f",distance);
    }
}

extension MainViewController : SlideMenuControllerDelegate {
    
    func leftWillOpen() {
        print("SlideMenuControllerDelegate: leftWillOpen")
    }
    
    func leftDidOpen() {
        print("SlideMenuControllerDelegate: leftDidOpen")
    }
    
    func leftWillClose() {
        print("SlideMenuControllerDelegate: leftWillClose")
    }
    
    func leftDidClose() {
        print("SlideMenuControllerDelegate: leftDidClose")
    }
    
    func rightWillOpen() {
        print("SlideMenuControllerDelegate: rightWillOpen")
    }
    
    func rightDidOpen() {
        print("SlideMenuControllerDelegate: rightDidOpen")
    }
    
    func rightWillClose() {
        print("SlideMenuControllerDelegate: rightWillClose")
    }
    
    func rightDidClose() {
        print("SlideMenuControllerDelegate: rightDidClose")
    }
}
