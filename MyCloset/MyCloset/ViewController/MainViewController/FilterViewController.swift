//
//  FilterViewController.swift
//  MyCloset
//
//  Created by M on 21/03/18.
//  Copyright © 2018 YDSHaridham. All rights reserved.
//

import UIKit

class FilterCell0: UITableViewCell {
    @IBOutlet var lblName:UILabel!
    @IBOutlet var btnRadio:UIButton!
}

class FilterCell1: UITableViewCell {
    @IBOutlet var lblName:UILabel!
    @IBOutlet var btnRadio:UIButton!
}

class FilterCell2: UITableViewCell {
    @IBOutlet var lblName:UILabel!
    @IBOutlet var btnRadio:UIButton!
}

class FilterCell5: UITableViewCell {
    @IBOutlet var lblName:UILabel!
    @IBOutlet var btnRadio:UIButton!
}

class FilterCell6: UITableViewCell {
    @IBOutlet var lblName:UILabel!
    @IBOutlet var btnRadio:UIButton!
}

class FilterCell7: UITableViewCell {
    @IBOutlet var lblName:UILabel!
    @IBOutlet var btnRadio:UIButton!
}

class SliderCell: UITableViewCell {
    //@IBOutlet var sliderPrice:UISlider!
    @IBOutlet var rangeSlider: RangeSeekSlider!
    @IBOutlet var lblMinPrice:UILabel!
    @IBOutlet var lblMaxPrice:UILabel!
}

class FilterViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, SelectBrandProtocol {
    
    
    
    let arrHeaderName = ["Gender", "Type Of Clothing", "Brand", "Price", "Condition", "Size", "Type Of Trade"]
    
    let arrRow0 = ["Man", "Woman"]
    var arrRow0Selected = [""]
    
    //let arrRow1 = ["READY-TO-WEAR", "SHOES", "ACCESSORIES"]
    var arrRow1 = ["T-Shirts", "Pants", "Shoes", "Shirts", "Accessories", "Hats", "Belts", "Coats", "Other"]
    var arrRow1Selected = [""]
    
    var arrRow2 = [String]()
    var arrSelectedBrand = [Any]()
    //var arrRow2Selected = [String]()
    
    let arrRow3 = [""]
    var strPrice = ""
    //var arrSelectedBrand = [Any]()
    
    let arrRow4 = ["Never used", "As good as new", "Fine but used", "Worn"]
    var arrRow4Selected = [String]()
    
    var arrRow5 = ["XS", "S", "M", "L", "XL", "XXL"]
    var arrRow5Selected = [String]()
    
    let arrRow6 = ["Buy now", "Auction"]
    var arrRow6Selected = [""]
    
    
    //MANS
    let arrMReadyWearType = ["Knitwear", "Jackets & Coats", "Leather & Casual Jackets", "Suits", "Shirts", "Sweatshirts", "T-shirts & Polos", "Trousers & Shorts", "Denim", "Activewear"]
    let arrMShoesType = ["Moccasins & Loafers", "Lace Ups", "Boots", "Slippers", "Drivers", "Sandals & Thongs", "Sneakers", "Precious Skins"]
    let arrMAccessoriesType = ["Wallets & Small Accessories", "Belts", "Ties", "Scarves", "Hats & Gloves", "Watches", "Jewellery", "Eyewear", "Socks"]
    let arrMShoesSize = ["40", "41", "41.5", "42", "43", "43.5", "44", "44.5", "45", "45.5", "46", "47.5", "48", "50"]
    
    //WOMAN
    let arrWReadyWearType = ["Dresses","Jackets","Coats & Furs","Leather & Casual Jackets","Tops & Shirts","Sweaters & Cardigans","Sweatshirts & T-Shirts","Skirts","Pants & Shorts","Denim","Activewear"]
    let arrWShoesType = ["Pumps", "Sandals","Moccasins & Loafers","Ballerinas","Slippers & Mules","Boots & Booties","Espadrilles & Wedges", "Sneakers"]
    let arrWAccessoriesType = ["Luggage & Lifestyle Bags","Wallets & Small Accessories","Belts","Silks & Scarves","Hats & Gloves", "Jewellery","Watches","Eyewear","Socks & Tights","Runway Accessories"]
    let arrWShoesSize = ["35.5", "36", "37", "37.5", "38", "38.5", "39", "39.5", "40", "41", "41.5", "42", "43"]
    
    @IBOutlet var tblFilter:UITableView!
    
    var cellSlider:SliderCell!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Filter"
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "imgBack")?.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(tappedOnBack(_:)))
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.isTranslucent = false
        //self.navigationController?.navigationBar.barTintColor = Color.COLOR_NAVIGATION_BACKGROUND
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        //self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - SelectBrandProtocol
    func selectBrand(brandName: String, brandId: String) {
        
    }
    
    func selectMultipleBrand(arrBrand: [Any]) {
        print("Brand:\(arrBrand)")
        arrSelectedBrand = arrBrand
    }
    
    // MARK: - Button Tapped Event
    @IBAction func tappedOnBack(_ sender:UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func tappedOnRadioGender(_ sender:UIButton) {
        sender.isSelected = true
        arrRow0Selected = [arrRow0[Int(sender.accessibilityValue!)!]]
        arrRow1Selected = [""]
        if arrRow0Selected[0] == "Man"  {
            arrRow1 = arrMReadyWearType
        }else if arrRow0Selected[0] == "Woman" {
            arrRow1 = arrWReadyWearType
        }
        
        self.tblFilter.reloadData()
    }
    
    
    @IBAction func tappedOnRadioType(_ sender:UIButton) {
        
        if sender.isSelected {
            sender.isSelected = false
            arrRow1Selected.remove(arrRow1[sender.tag])
        }else {
            sender.isSelected = true
            if !arrRow1Selected.contains(arrRow1[sender.tag]){
                arrRow1Selected.append(arrRow1[sender.tag])
            }
        }
        self.tblFilter.reloadData()
        
    }
    
    @IBAction func tappedOnRadioCondition(_ sender:UIButton) {
        
        if sender.isSelected {
            sender.isSelected = false
            arrRow4Selected.remove(arrRow4[sender.tag])
        }else {
            sender.isSelected = true
            if !arrRow4Selected.contains(arrRow4[sender.tag]){
                arrRow4Selected.append(arrRow4[sender.tag])
            }
        }
        self.tblFilter.reloadData()
    }
    
    @IBAction func tappedOnRadioSize(_ sender:UIButton) {
        
            if sender.isSelected {
                sender.isSelected = false
                arrRow5Selected.remove(arrRow5[sender.tag])
            }else {
                sender.isSelected = true
                if !arrRow5Selected.contains(arrRow5[sender.tag]){
                    arrRow5Selected.append(arrRow5[sender.tag])
                }
            }
            self.tblFilter.reloadData()
        
    }
    
    @IBAction func tappedOnRadioTrade(_ sender:UIButton) {
        sender.isSelected = true
        arrRow6Selected = [arrRow6[sender.tag]]
        
        
        self.tblFilter.reloadData()
    }
    
    @IBAction func tappedOnBrand(_ sender:UIButton) {
        let brandView = self.storyboard?.instantiateViewController(withIdentifier: "BrandViewController") as! BrandViewController
        brandView.delegate = self
        brandView.isSelectMultiple = true
        self.navigationController?.pushViewController(brandView, animated: true)
    }
    
    @IBAction func tappedOnApply(_ sender:UIButton) {
        let strGender = arrRow0Selected[0]
        //let strCategory = arrRow1Selected[0]
        
        var strType = ""
        if arrRow1Selected.count > 0 {
            for name in arrRow1Selected {
                strType = strType + ",\(name)"
            }
        }
        if strType.length > 0 {
            strType.removeFirst(1)
        }
        
        var strCondition = ""
        if arrRow4Selected.count > 0 {
            for name in arrRow4Selected {
                strCondition = strCondition + ",\(name)"
            }
        }
        if strCondition.length > 0 {
            strCondition.removeFirst(1)
        }
        
        var strSize = ""
        if arrRow5Selected.count > 0 {
            for name in arrRow5Selected {
                strSize = strSize + ",\(name)"
            }
        }
        if strSize.length > 0 {
            strSize.removeFirst(1)
        }
        
        
        var strTrade = ""
        if arrRow6Selected[0] == "Buy now" {
            strTrade = "0"
        }else if arrRow6Selected[0] == "Auction" {
            strTrade = "1"
        }
        
        var strBrand = ""
        if arrSelectedBrand.count > 0 {
            for dict in arrSelectedBrand {
                let dictTemp = dict as! [String:String]
                strBrand = strBrand + ",\(dictTemp["Id"] as! String)"
            }
        }
        if strBrand.length > 0 {
            strBrand.removeFirst(1)
        }
        
        print("Gender:\(strGender)")
        //print("Category:\(strCategory)")
        print("Type:\(strType)")
        print("Condition:\(strCondition)")
        print("Size:\(strSize)")
        print("Trade:\(strTrade)")
        print("Brand:\(strBrand)")
        
        setMyUserDefaults(value: strPrice, key: MyUserDefaults.FilterPrice)
        setMyUserDefaults(value: strGender, key: MyUserDefaults.FilterGender)
        //setMyUserDefaults(value: strCategory, key: MyUserDefaults.FilterCategory)
        setMyUserDefaults(value: strType, key: MyUserDefaults.FilterType)
        setMyUserDefaults(value: strCondition, key: MyUserDefaults.FilterCondition)
        setMyUserDefaults(value: strSize, key: MyUserDefaults.FilterSize)
        setMyUserDefaults(value: strTrade, key: MyUserDefaults.FilterTrade)
        setMyUserDefaults(value: strBrand, key: MyUserDefaults.FilterBrand)
        UserDefaults.standard.set(true, forKey: MyUserDefaults.isFilterApply)
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func tappedOnRest(_ sender:UIButton) {
        setMyUserDefaults(value: "", key: MyUserDefaults.FilterGender)
        setMyUserDefaults(value: "", key: MyUserDefaults.FilterCategory)
        setMyUserDefaults(value: "", key: MyUserDefaults.FilterType)
        setMyUserDefaults(value: "", key: MyUserDefaults.FilterCondition)
        setMyUserDefaults(value: "", key: MyUserDefaults.FilterSize)
        setMyUserDefaults(value: "", key: MyUserDefaults.FilterTrade)
        setMyUserDefaults(value: "", key: MyUserDefaults.FilterBrand)
        UserDefaults.standard.set(false, forKey: MyUserDefaults.isFilterApply)
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @objc func updatePriceLabel(sender: UISlider!) {
        let value = Int(sender.value)
        //cellSlider.lblCurrentPrice.text = "\(value)"
        strPrice = "\(value)"
        
    }
    
    //MARK: - UITableViewDataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrHeaderName.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return self.arrRow0.count
        }else if section == 1{
            return self.arrRow1.count
        }else if section == 2{
            return self.arrRow2.count
        }else if section == 3{
            return self.arrRow3.count
        }else if section == 4{
            return self.arrRow4.count
        }else if section == 5{
            return self.arrRow5.count
        }else if section == 6{
            return self.arrRow6.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FilterCell0") as! FilterCell0
            cell.lblName.text = self.arrRow0[indexPath.row]
            if arrRow0Selected[0] == self.arrRow0[indexPath.row] {
                cell.btnRadio.isSelected = true
            }else {
                cell.btnRadio.isSelected = false
            }
            cell.btnRadio.tag = indexPath.row
            cell.btnRadio.accessibilityValue = "\(indexPath.row)"
            cell.btnRadio.addTarget(self, action: #selector(tappedOnRadioGender(_:)), for: .touchUpInside)
            cell.selectionStyle = .none
            return cell
        }else if indexPath.section == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "FilterCell1") as! FilterCell1
            cell.lblName.text =  self.arrRow1[indexPath.row]
            if arrRow1Selected.contains(self.arrRow1[indexPath.row]) {
                cell.btnRadio.isSelected = true
            }else{
                cell.btnRadio.isSelected = false
            }
            cell.btnRadio.tag = indexPath.row
            cell.btnRadio.addTarget(self, action: #selector(tappedOnRadioType(_:)), for: .touchUpInside)
            cell.selectionStyle = .none
            return cell
        }/*else if indexPath.section == 2{
             let cell = tableView.dequeueReusableCell(withIdentifier: "FilterCell2") as! FilterCell2
             cell.lblName.text =  self.arrRow2[indexPath.row]
             if arrRow2Selected.contains(self.arrRow2[indexPath.row]) {
             cell.btnRadio.isSelected = true
             }else{
             cell.btnRadio.isSelected = false
             }
             cell.btnRadio.tag = indexPath.row
             cell.btnRadio.addTarget(self, action: #selector(tappedOnRadioType(_:)), for: .touchUpInside)
             cell.selectionStyle = .none
             return cell
         }*/else if indexPath.section == 2{
            let cell = tableView.dequeueReusableCell(withIdentifier: "FilterCell2") as! FilterCell2
            cell.lblName.text =  self.arrRow3[indexPath.row]
            cell.selectionStyle = .none
            return cell
        }else if indexPath.section == 3{
            cellSlider = tableView.dequeueReusableCell(withIdentifier: "SliderCell") as! SliderCell
            cellSlider.selectionStyle = .none
            
            cellSlider.rangeSlider.handleColor = UIColor(patternImage: UIImage(named: "imgGoldanBG")!)
            cellSlider.rangeSlider.tintColor = UIColor(patternImage: UIImage(named: "slider")!)
            cellSlider.rangeSlider.colorBetweenHandles = UIColor(patternImage: UIImage(named: "slider")!)
            //cellSlider.rangeSlider
            cellSlider.rangeSlider.delegate = self
            //cellSlider.sliderPrice.addTarget(self, action: #selector(updatePriceLabel(sender:)), for: .valueChanged)
            return cellSlider
        }else if indexPath.section == 4{
            let cell = tableView.dequeueReusableCell(withIdentifier: "FilterCell5") as! FilterCell5
            cell.lblName.text =  self.arrRow4[indexPath.row]
            if arrRow4Selected.contains(self.arrRow4[indexPath.row]) {
                cell.btnRadio.isSelected = true
            }else{
                cell.btnRadio.isSelected = false
            }
            cell.btnRadio.tag = indexPath.row
            cell.btnRadio.addTarget(self, action: #selector(tappedOnRadioCondition(_:)), for: .touchUpInside)
            cell.selectionStyle = .none
            return cell
        }else if indexPath.section == 5{
            let cell = tableView.dequeueReusableCell(withIdentifier: "FilterCell6") as! FilterCell6
            cell.lblName.text =  self.arrRow5[indexPath.row]
            if arrRow5Selected.contains(self.arrRow5[indexPath.row]) {
                cell.btnRadio.isSelected = true
            }else{
                cell.btnRadio.isSelected = false
            }
            cell.btnRadio.tag = indexPath.row
            cell.btnRadio.addTarget(self, action: #selector(tappedOnRadioSize(_:)), for: .touchUpInside)
            cell.selectionStyle = .none
            return cell
        }else if indexPath.section == 6{
            let cell = tableView.dequeueReusableCell(withIdentifier: "FilterCell7") as! FilterCell7
            cell.lblName.text =  self.arrRow6[indexPath.row]
            if arrRow6Selected.contains(self.arrRow6[indexPath.row]) {
                cell.btnRadio.isSelected = true
            }else{
                cell.btnRadio.isSelected = false
            }
            cell.btnRadio.tag = indexPath.row
            cell.btnRadio.addTarget(self, action: #selector(tappedOnRadioTrade(_:)), for: .touchUpInside)
            cell.selectionStyle = .none
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 3 {
            return 70.0
        }else {
            return 44.0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: ScreenSize.SCREEN_WIDTH, height: 44))
        headerView.backgroundColor = Color.VIEWBACKGROUND
        
        let subView = UIView(frame: CGRect(x: 0.0, y: 5.0, width: ScreenSize.SCREEN_WIDTH, height: 39))
        subView.backgroundColor = UIColor.white
        headerView.addSubview(subView)
        
        let lblName = UILabel(frame: CGRect(x: 10, y: 5, width: ScreenSize.SCREEN_WIDTH - 30, height: 35))
        lblName.text = arrHeaderName[section]
        lblName.font = UIFont(name: FontName.CoreSansGMedium, size: 17.0)
        subView.addSubview(lblName)
        
        if section == 2 {
            let btnTap = UIButton(frame: CGRect(x: 10, y: 5, width: ScreenSize.SCREEN_WIDTH - 30, height: 35))
            btnTap.addTarget(self, action: #selector(tappedOnBrand(_:)), for: .touchUpInside)
            subView.addSubview(btnTap)
        }
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44.0
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension FilterViewController: RangeSeekSliderDelegate {
    
    func rangeSeekSlider(_ slider: RangeSeekSlider, didChange minValue: CGFloat, maxValue: CGFloat) {
       print("Custom slider updated. Min Value: \(minValue) Max Value: \(maxValue)")
        cellSlider.lblMinPrice.text = "\(Int(minValue))"
        cellSlider.lblMaxPrice.text = "\(Int(maxValue))"
        strPrice = "\(Int(minValue))"
    }
    
    func didStartTouches(in slider: RangeSeekSlider) {
        print("did start touches")
    }
    
    func didEndTouches(in slider: RangeSeekSlider) {
        print("did end touches")
    }
}


extension Array where Element:Equatable {
    public mutating func remove(_ item:Element ) {
        var index = 0
        while index < self.count {
            if self[index] == item {
                self.remove(at: index)
            } else {
                index += 1
            }
        }
    }
    
    public func array( removing item:Element ) -> [Element] {
        var result = self
        result.remove( item )
        return result
    }
}
