//
//  RecentViewController.swift
//  MyCloset
//
//  Created by M on 27/03/18.
//  Copyright © 2018 YDSHaridham. All rights reserved.
//

import UIKit

class MessageCell: UITableViewCell {
    @IBOutlet var imgViewUser:UIImageView!
    @IBOutlet var lblUserName:UILabel!
    @IBOutlet var lblProdName:UILabel!
    @IBOutlet var lblTime:UILabel!
}

class RecentViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet var tblMessage:UITableView!
    
    private var newMessageRefHandle: FIRDatabaseHandle?
    var messageRef: FIRDatabaseReference!
    
    var arrRecentMessage = [Any]()
    override func awakeFromNib() {
        self.tabBarItem.title = "Messages"
        self.title = "Messages"

        //self.navigationController?.navigationBar.isTranslucent = false
        //self.navigationController?.navigationBar.barTintColor = Color.COLOR_NAVIGATION_BACKGROUND
        self.addLeftBarMenuWithImage(#imageLiteral(resourceName: "imgMenu").withRenderingMode(.alwaysOriginal))
        //self.tabBarItem.image = UIImage(named: "imgMessage")
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        observeRecents()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        setMyUserDefaults(value: "0", key: MyUserDefaults.NotificationCount)
        self.navigationController?.navigationBar.isTranslucent = false
        //self.navigationController?.navigationBar.barTintColor = Color.COLOR_NAVIGATION_BACKGROUND
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        //self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
    }
    
    deinit {
        if let refHandle = newMessageRefHandle {
            messageRef.removeObserver(withHandle: refHandle)
        }
    }
    
    private func observeRecents() {
        let userDetail = getMyUserDefaults(key: MyUserDefaults.UserData) as! [String:Any]
        //appDelegate.showHud()
        messageRef = FIRDatabase.database().reference(withPath: FRECENT_PATH)
        
        
        let messageQuery = messageRef.queryOrdered(byChild: FRECENT_USERID).queryEqual(toValue: userDetail["email"])
        messageQuery.observe(FIRDataEventType.childAdded) { (snapshot) in
            let messageData = snapshot.value as! [String:Any]
            //appDelegate.hideHud()
            print(messageData)
            
            self.arrRecentMessage.append(messageData)
            self.tblMessage.reloadData()
        }
    }
    
    //MARK: - Methods
    func addLeftBarMenuWithImage(_ buttonImage: UIImage) {
        let leftButton: UIBarButtonItem = UIBarButtonItem(image: buttonImage, style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.toggleLeftMenu))
        navigationItem.leftBarButtonItem = leftButton
    }
    
    @objc public func toggleLeftMenu() {
        slideMenuController()?.toggleLeft()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - UITableViewDataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.arrRecentMessage.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MessageCell") as! MessageCell
        let dictRecent = self.arrRecentMessage[indexPath.section] as! [String:Any]
        //let userDetail = getMyUserDefaults(key: MyUserDefaults.UserData) as! [String:Any]
        
        cell.lblUserName.text = dictRecent["name"] as? String
        cell.imgViewUser.sd_setImage(with: URL(string: dictRecent["photo"] as! String)) { (image, error, nil, url) in
            if image == nil {
                cell.imgViewUser.image = #imageLiteral(resourceName: "imgUser")
            }
        }
        
        
        //cell.imgViewUser.sd_setImage(with: URL(string: dictRecent["photo"] as! String), completed:)
        cell.lblProdName.text = ""
        cell.lblTime.text = TimeElapsed(dictRecent[FRECENT_UPDATEDAT] as! Int64)
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 10
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        /*FIRAuth.auth()?.signInAnonymously(completion: { (user, error) in
            if let err:Error = error {
                print(err.localizedDescription)
                return
            }
            let userDetail = getMyUserDefaults(key: MyUserDefaults.UserData) as! [String:Any]
            let chatView = self.storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
            chatView.hidesBottomBarWhenPushed = true
            let dictRecent = self.arrRecentMessage[indexPath.section] as! [String:Any]
            chatView.messageObjectID = dictRecent[FRECENT_MSGID] as! String
            if dictRecent["userId"] as! String == userDetail["user_id"] as! String {
                chatView.senderDisplayName = dictRecent["name"] as! String
                chatView.userID = dictRecent["userId"] as! String
                chatView.userEmail = dictRecent["currentEmail"] as! String
            }
            if dictRecent["currentUserId"] as! String == userDetail["user_id"] as! String {
                chatView.senderDisplayName = dictRecent["currentName"] as! String
                chatView.userID = dictRecent["userId"] as! String
                chatView.userEmail = dictRecent["currentEmail"] as! String
            }
            self.navigationController?.pushViewController(chatView, animated: true)
        })*/
    }
}
