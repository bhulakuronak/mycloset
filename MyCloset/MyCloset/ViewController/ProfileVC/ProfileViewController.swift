//
//  ProfileViewController.swift
//  MyCloset
//
//  Created by M on 19/03/18.
//  Copyright © 2018 YDSHaridham. All rights reserved.
//

import UIKit
import Alamofire
import CoreLocation
class ProfileViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate {

    @IBOutlet var scrollView:UIScrollView!
    
    @IBOutlet var txtName:UITextField!
    @IBOutlet var txtLocation:UITextField!
    @IBOutlet var txtPassword:UITextField!
    @IBOutlet var txtPhone:UITextField!
    @IBOutlet var txtAge:UITextField!
    @IBOutlet var txtGender:UITextField!
    @IBOutlet var txtStreet:UITextField!
    @IBOutlet var txtHouseNumber:UITextField!
    @IBOutlet var txtZipCode:UITextField!
    
    @IBOutlet var imgViewProfile:UIImageView!
    @IBOutlet var imgCamera:UIImageView!
    @IBOutlet var cntrolProfile:UIControl!
    var strTextFieldSelected:String = ""
    var userProfile:UIImage!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //------Navigation Button------
        self.title = "Profile"
        self.navigationController?.navigationBar.isTranslucent = false
        //self.navigationController?.navigationBar.barTintColor = Color.COLOR_NAVIGATION_BACKGROUND
        self.addLeftBarMenuWithImage(#imageLiteral(resourceName: "imgMenu").withRenderingMode(.alwaysOriginal))
        
        if let dictUser = getMyUserDefaults(key: MyUserDefaults.UserData) as? [String:Any] {
            print("UserDetail\(dictUser)")
            self.txtName.text = dictUser["name"] as? String
            self.txtLocation.text = dictUser["address"] as? String
            self.txtPassword.text = ""
            self.txtPhone.text = dictUser["phonenumber"] as? String
            self.txtAge.text = dictUser["age"] as? String
            self.txtGender.text = dictUser["gender"] as? String
            self.txtStreet.text = dictUser["Street_name"] as? String
            self.txtHouseNumber.text = dictUser["houseName"] as? String
            self.txtZipCode.text = dictUser["zipcode"] as? String
            let imgUrl = dictUser["profile_picture"] as! String
            
            
            imgViewProfile.sd_setImage(with: URL(string: imgUrl), placeholderImage: #imageLiteral(resourceName: "imgUser"), options: .allowInvalidSSLCertificates) { (image, error, nil, url) in
                if image != nil {
                    self.imgCamera.isHidden = true
                }
            }
        }
        self.getUserProfile()
        
        // Do any additional setup after loading the view.
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        registerNotifications()
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        unregisterNotifications()
    }
    
    
    //MARK: - NotificationCenter Register
    func registerNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: .UIKeyboardWillHide, object: nil)
    }
    
    func unregisterNotifications() {
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillHide, object: nil)
    }
    
    @objc func keyboardWillShow(notification: NSNotification){
        guard let keyboardFrame = notification.userInfo![UIKeyboardFrameBeginUserInfoKey] as? NSValue else { return }
        scrollView.contentInset.bottom = view.convert(keyboardFrame.cgRectValue, from: nil).size.height + 20
    }
    
    @objc func keyboardWillHide(notification: NSNotification){
        scrollView.contentInset.bottom = 0
    }
    
    //MARK: - Methods
    func addLeftBarMenuWithImage(_ buttonImage: UIImage) {
        let leftButton: UIBarButtonItem = UIBarButtonItem(image: buttonImage, style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.toggleLeftMenu))
        navigationItem.leftBarButtonItem = leftButton
    }
    
    @objc public func toggleLeftMenu() {
        slideMenuController()?.toggleLeft()
    }
    
    func getUserProfile() {
        let userDetail = getMyUserDefaults(key: MyUserDefaults.UserData) as! [String:Any]
        
        let parameters = ["i_key":WebURL.appKey,
                          "user_id":userDetail["user_id"] ?? ""] as [String : Any]
        appDelegate.showHud()
        request(WebURL.showProfile, method: .post, parameters: parameters).responseJSON(completionHandler: { (response) in
            print("showProfile Result:\(response)")
            appDelegate.hideHud()
            if response.result.isSuccess {
                if let dictResult:[String:Any] = response.result.value as! [String : Any]? {
                    if (dictResult["o_returnCode"] as! Bool){
                        setMyUserDefaults(value: dictResult["o_data"] as! [String:Any], key: MyUserDefaults.UserData)
                    }else{
                        let errorMsg = dictResult["o_message"] as! String
                        appDelegate.showAlertMessage(strMessage: errorMsg)
                    }
                    
                }
            }
            //self.presentingViewController?.dismiss(animated: true, completion: nil)
        })
        
    }
    
    func updateProfileDetail() {
        let userDetail = getMyUserDefaults(key: MyUserDefaults.UserData) as! [String:Any]
        
        var parameters = [String:String]()
        parameters["i_key"] = WebURL.appKey
        parameters["name"] = txtName.text ?? ""
        parameters["password"] = txtPassword.text ?? ""
        parameters["phonenumber"] = txtPhone.text ?? ""
        parameters["zipcode"] = txtZipCode.text ?? ""
        parameters["age"] = txtAge.text ?? ""
        parameters["gender"] = txtGender.text ?? ""
        parameters["Street_name"] = txtStreet.text ?? ""
        parameters["houseName"] = txtHouseNumber.text ?? ""
        parameters["user_id"] = (userDetail["user_id"] as! String)
        
        var strLat = ""
        var strLong = ""
        if let latitude = getMyUserDefaults(key: MyUserDefaults.UserLatitude) as? CLLocationDegrees {
            let longitude = getMyUserDefaults(key: MyUserDefaults.UserLongitude) as! CLLocationDegrees
            strLat = "\(latitude)"
            strLong = "\(longitude)"
            parameters["lat"] = strLat
            parameters["long"] = strLong
            
        }
        appDelegate.showHud()
        
        
        upload(multipartFormData: { (multipartFormData) in
            if self.userProfile != nil {
                let data1 = UIImageJPEGRepresentation(self.userProfile!, 7.0)
                multipartFormData.append(data1!, withName: "profile_picture", fileName: "userProfile.jpg", mimeType: "image/jpeg")
            }
            for (key, value) in parameters {
                multipartFormData.append((value as! String).data(using: .utf8)!, withName: key)
            }
            
        }, to: WebURL.updateProfile, method: .post, encodingCompletion: { (encodingResult) in
            switch encodingResult {
            case .success(let upload, _, _):
                
                
                upload.validate()
                upload.responseJSON { response in
                    
                    appDelegate.hideHud()
                    if response.result.error != nil {
                        print("failure")
                        UIAlertView(title: "Fail", message: "Please retry again.", delegate: nil, cancelButtonTitle: "Ok").show()
                    } else {
                        print("success")
                        print("updateProfile Result:\(response)")
                        if let dictResult:[String:Any] = response.result.value as! [String : Any]? {
                            if (dictResult["o_returnCode"] as! Bool){
                                let successMsg = dictResult["o_message"] as! String
                                showAlert(msg: successMsg, completion: { (true) in
                                    setMyUserDefaults(value: dictResult["o_data"] as! [String:Any], key: MyUserDefaults.UserData)
                                    self.navigationController?.popViewController(animated: true)
                                })
                            }else{
                                let errorMsg = dictResult["o_message"] as! String
                                appDelegate.showAlertMessage(strMessage: errorMsg)
                            }
                        }
                    }
                }
                
            case .failure(let encodingError):
                print(encodingError)
                //failure
            }
        })
    }
    
    // MARK: - Button Tapped Event
    @IBAction func tappedOnBack(_ sender:UIBarButtonItem)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func tappedOnProfile(_ sender:UIControl)
    {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let action1 = UIAlertAction(title: "Open Camera", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            _ = PresentPhotoCamera(target: self, canEdit: true)
        })
        let action2 = UIAlertAction(title: "Photo Library", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            _ = PresentPhotoLibrary(target: self, canEdit: true)
        })
        let action3 = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alert.addAction(action1)
        alert.addAction(action2)
        alert.addAction(action3)
        present(alert, animated: true) {
            
        }
    }
    
    @IBAction func tappedOnSave(_ sender:UIButton) {
        updateProfileDetail()
    }
    
    //MARK: - UITextFieldDelegate
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtName {
            strTextFieldSelected = "txtName"
        }else if textField == txtLocation  {
            strTextFieldSelected = "txtLocation"
        }else if textField == txtPassword  {
            strTextFieldSelected = "txtPassword"
        }else if textField == txtPhone  {
            strTextFieldSelected = "txtPhone"
        }
        
        //textField.inputAccessoryView = self.toolbarInit()
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    
    // MARK: - Keyboard
    func toolbarInit() -> UIToolbar
    {
        let toolBar = UIToolbar()
        toolBar.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 40)
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.barTintColor = Color.keyboardHeaderColor
        toolBar.tintColor = UIColor.white
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(resignKeyboard))
        let previousButton:UIBarButtonItem! = UIBarButtonItem()
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        
        previousButton.customView = self.prevNextSegment()
        toolBar.setItems([previousButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        toolBar.sizeToFit()
        
        return toolBar;
    }
    
    func prevNextSegment() -> UISegmentedControl
    {
        let prevNextSegment = UISegmentedControl()
        prevNextSegment.isMomentary = true
        prevNextSegment.tintColor = UIColor.white
        let barbuttonFont = UIFont(name: FontName.OpenSansRegular, size: 15) ?? UIFont.systemFont(ofSize: 15)
        prevNextSegment.setTitleTextAttributes([NSAttributedStringKey.font: barbuttonFont, NSAttributedStringKey.foregroundColor:UIColor.clear], for: UIControlState.disabled)
        if(DeviceType.IS_IPHONE_6PLUS)
        {
            prevNextSegment.frame = CGRect(x: 0, y: 0, width: 130, height: 28) //CGRectMake(0,0,130, 28)
        }
        else
        {
            prevNextSegment.frame = CGRect(x: 0, y: 0, width: 130, height: 40) //CGRectMake(0,0,130, 40)
        }
        prevNextSegment.insertSegment(withTitle: "Previous", at: 0, animated: false)
        prevNextSegment.insertSegment(withTitle: "Next", at: 1, animated: false)
        
        prevNextSegment.addTarget(self, action: #selector(prevOrNext), for: UIControlEvents.valueChanged)
        return prevNextSegment;
    }
    
    @objc func prevOrNext(segm: UISegmentedControl)
    {
        if (segm.selectedSegmentIndex == 1)
        {
            if strTextFieldSelected == "txtName" {
                txtLocation.becomeFirstResponder()
            }else if strTextFieldSelected == "txtLocation" {
                txtPassword.becomeFirstResponder()
            }else if strTextFieldSelected == "txtPassword" {
                txtPhone.becomeFirstResponder()
            }else if strTextFieldSelected == "txtPhone" {
                txtPhone.resignFirstResponder()
            }
        }else
        {
            if strTextFieldSelected == "txtName" {
                txtName.resignFirstResponder()
            }else if strTextFieldSelected == "txtLocation" {
                txtName.becomeFirstResponder()
            }else if strTextFieldSelected == "txtPassword" {
                txtLocation.becomeFirstResponder()
            }else if strTextFieldSelected == "txtPhone" {
                txtPassword.becomeFirstResponder()
            }
        }
    }
    
    @objc func resignKeyboard()
    {
        self.view.endEditing(true)
        
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        resignKeyboard()
    }
    
    //MARK: -
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        // use the image
        userProfile = chosenImage
        imgViewProfile.image = chosenImage
        imgCamera.isHidden = true
        dismiss(animated: true, completion: nil)
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
