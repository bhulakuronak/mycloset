//
//  GuideViewController.swift
//  MyCloset
//
//  Created by Hari Prabodhm on 01/03/19.
//  Copyright © 2019 YDSHaridham. All rights reserved.
//

import UIKit

class GuideViewController: UIViewController, UIScrollViewDelegate {

    @IBOutlet var scrollView:UIScrollView!
    @IBOutlet var pageControl:UIPageControl!
    @IBOutlet var  btnNext:UIButton!
    @IBOutlet var heightConstraint:NSLayoutConstraint!
    
    var isPresentView = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        var bottomPadding: CGFloat = 0.0
        if #available(iOS 11.0, *) {
            let window = UIApplication.shared.keyWindow
            bottomPadding = window?.safeAreaInsets.bottom ?? 0.0
        }
        let statusHeigt = UIApplication.shared.statusBarFrame.height
        if isPresentView {
            heightConstraint.constant = ScreenSize.SCREEN_HEIGHT
        }else {
            heightConstraint.constant = ScreenSize.SCREEN_HEIGHT - statusHeigt - bottomPadding
        }
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func tappedOnSkip(_ sender: UIButton) {
        if isPresentView {
            self.dismiss(animated: true, completion: nil)
        }else{
        let mainView = self.storyboard?.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
        let nvMain: NavigationController = NavigationController(rootViewController: mainView)
        self.slideMenuController()?.changeMainViewController(nvMain, close: true)
        }
    }
    
    @IBAction func tappedOnNext(_ sender: UIButton) {
        if sender.title(for: .normal) == nil || sender.title(for: .normal) == "" {
            pageControl.currentPage = Int(pageControl.currentPage + 1)
            self.scrollToPage(page: pageControl.currentPage, animated: true)
            if pageControl.currentPage == 4 {
                btnNext.setTitle("FINISH", for: .normal)
                btnNext.setImage(nil, for: .normal)
            }else{
                btnNext.setTitle("", for: .normal)
                btnNext.setImage(UIImage(named: "imgArrowRight"), for: .normal)
            }
        }else if sender.title(for: .normal) == "FINISH" {
            if isPresentView {
                self.dismiss(animated: true, completion: nil)
            }else{
                let mainView = self.storyboard?.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
                let nvMain: NavigationController = NavigationController(rootViewController: mainView)
                self.slideMenuController()?.changeMainViewController(nvMain, close: true)
            }
        }
    }
    
    func scrollToPage(page: Int, animated: Bool) {
        var frame: CGRect = self.scrollView.frame
        frame.origin.x = frame.size.width * CGFloat(page)
        frame.origin.y = 0
        self.scrollView.scrollRectToVisible(frame, animated: animated)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        if pageNumber == 4 {
            btnNext.setTitle("FINISH", for: .normal)
            btnNext.setImage(nil, for: .normal)
        }else{
            btnNext.setTitle("", for: .normal)
            btnNext.setImage(UIImage(named: "imgArrowRight"), for: .normal)
        }
        pageControl.currentPage = Int(pageNumber)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
