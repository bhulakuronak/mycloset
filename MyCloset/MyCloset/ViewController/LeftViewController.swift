//
//  LeftViewController.swift
//  SlideMenuControllerSwift
//
//  Created by Yuji Hato on 12/3/14.
//

import UIKit
import GoogleSignIn

class ProfileCell: UITableViewCell {
    @IBOutlet var imgProfile:UIImageView!
    @IBOutlet var lblUserName:UILabel!
}

class MenuCell: UITableViewCell {
    @IBOutlet var lblTitle:UILabel!
    @IBOutlet var imgUpDownArrow:UIImageView!
}

class SettingSubMenu: UITableViewCell {
    @IBOutlet var lblTitle:UILabel!
    @IBOutlet var switchOnOff:UISwitch!
}

enum LeftMenu: Int {
    case main = 0
    case swift
    case java
    case go
    case nonMenu
}

protocol LeftMenuProtocol : class {
    func changeViewController(_ menu: LeftMenu)
}

class LeftViewController : UIViewController, LeftMenuProtocol {
    
    @IBOutlet weak var tableView: UITableView!
    var menus = ["", "Home", "Profile", "Chat", "Order History", "Notification Setting", "Quickstart Guide", "Customer Support", "Logout"]
    var menusWithSubmenu = ["", "Home", "Profile", "Chat", "Order History", "Notification Setting", "New Messages", "Intrested Buyers", "Guide", "Customer Support", "Logout"]
    var mainViewController: UIViewController!
    var swiftViewController: UIViewController!
    var javaViewController: UIViewController!
    var goViewController: UIViewController!
    var nonMenuViewController: UIViewController!
    //var imageHeaderView: ImageHeaderView!
    var isSettingOpen:Bool = false
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.separatorColor = UIColor(red: 224/255, green: 224/255, blue: 224/255, alpha: 1.0)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tableView.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
    }
    
    func changeViewController(_ menu: LeftMenu) {
        switch menu {
        case .main:
            self.slideMenuController()?.changeMainViewController(self.mainViewController, close: true)
        case .swift:
            self.slideMenuController()?.changeMainViewController(self.swiftViewController, close: true)
        case .java:
            self.slideMenuController()?.changeMainViewController(self.javaViewController, close: true)
        case .go:
            self.slideMenuController()?.changeMainViewController(self.goViewController, close: true)
        case .nonMenu:
            self.slideMenuController()?.changeMainViewController(self.nonMenuViewController, close: true)
        }
    }
}

extension LeftViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 200
        }else{
            return 50
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 5 {
            if isSettingOpen {
                isSettingOpen = false
            }else{
                isSettingOpen = true
            }
            self.tableView.reloadData()
        }else if indexPath.row == 1 {
            let mainView = self.storyboard?.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
            let nvMain: NavigationController = NavigationController(rootViewController: mainView)
            self.slideMenuController()?.changeMainViewController(nvMain, close: true)
        }else if indexPath.row == 2 {
            let profileView = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
            let nvProfile: NavigationController = NavigationController(rootViewController: profileView)
            self.slideMenuController()?.changeMainViewController(nvProfile, close: true)
        }else if indexPath.row == 3 {
            let recentVC = self.storyboard?.instantiateViewController(withIdentifier: "RecentViewController") as! RecentViewController
            let nvRecent: NavigationController = NavigationController(rootViewController: recentVC)
            self.slideMenuController()?.changeMainViewController(nvRecent, close: true)
        }else if indexPath.row == 4 {
            let orderHistory = self.storyboard?.instantiateViewController(withIdentifier: "OrderHistoryVC") as! OrderHistoryVC
            let nvOrder: NavigationController = NavigationController(rootViewController: orderHistory)
            self.slideMenuController()?.changeMainViewController(nvOrder, close: true)
        }
        if isSettingOpen {
            if indexPath.row == 8 {
                let guideVC = self.storyboard?.instantiateViewController(withIdentifier: "GuideViewController") as! GuideViewController
                let nvOrder: NavigationController = NavigationController(rootViewController: guideVC)
                nvOrder.isNavigationBarHidden = true
                self.slideMenuController()?.changeMainViewController(nvOrder, close: true)
            }else if indexPath.row == 9 {
                
            }else if indexPath.row == 10 {
                
                let alertController = UIAlertController(title: "LOGOUT", message: "Are you sure you want to logout?", preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "Logout", style: .default, handler: {
                    alert -> Void in
                    //setMyUserDefaults(value: nil, key: MyUserDefaults.UserData)
                    setMyUserDefaults(value: false, key: MyUserDefaults.isLogin)
                    self.dismiss(animated: true, completion: {
                        GIDSignIn.sharedInstance().signOut()
                    })
                    let viewController = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                    let nvViewController: NavigationController = NavigationController(rootViewController: viewController)
                    self.slideMenuController()?.changeMainViewController(nvViewController, close: true)
                }))
                alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
                
                self.present(alertController, animated: true, completion: nil)
                
                
            }
        }else {
            if indexPath.row == 6 {
                let guideVC = self.storyboard?.instantiateViewController(withIdentifier: "GuideViewController") as! GuideViewController
                let nvOrder: NavigationController = NavigationController(rootViewController: guideVC)
                nvOrder.isNavigationBarHidden = true
                self.slideMenuController()?.changeMainViewController(nvOrder, close: true)
            }else if indexPath.row == 7 {
                
            }else if indexPath.row == 8 {
                
                let alertController = UIAlertController(title: "LOGOUT", message: "Are you sure you want to logout?", preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "Logout", style: .default, handler: {
                    alert -> Void in
                    //setMyUserDefaults(value: nil, key: MyUserDefaults.UserData)
                    setMyUserDefaults(value: false, key: MyUserDefaults.isLogin)
                    self.dismiss(animated: true, completion: {
                        GIDSignIn.sharedInstance().signOut()
                    })
                    let viewController = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                    let nvViewController: NavigationController = NavigationController(rootViewController: viewController)
                    self.slideMenuController()?.changeMainViewController(nvViewController, close: true)
                }))
                alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
                
                self.present(alertController, animated: true, completion: nil)
                
                
            }
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if self.tableView == scrollView {
            
        }
    }
}

extension LeftViewController : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSettingOpen {
            return menusWithSubmenu.count
        }else{
            return menus.count
        }
        
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileCell") as! ProfileCell
            let userDetail = getMyUserDefaults(key: MyUserDefaults.UserData) as! [String:Any]
            let imgURL = userDetail["profile_picture"] as! String
            cell.imgProfile.sd_setImage(with: URL(string: imgURL), completed: { (image, error, nil, url) in
                if image == nil {
                    cell.imgProfile.image = #imageLiteral(resourceName: "imgUser")
                }
            })
            cell.lblUserName.text = userDetail["name"] as? String
            return cell
        }else{
            if isSettingOpen {
                if indexPath.row == 6 || indexPath.row == 7 {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "SettingSubMenu") as! SettingSubMenu
                    cell.lblTitle.text = menusWithSubmenu[indexPath.row]
                    cell.lblTitle.lineBreakMode = .byCharWrapping
                    return cell
                }else{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "MenuCell") as! MenuCell
                    cell.lblTitle.text = menusWithSubmenu[indexPath.row]
                    cell.lblTitle.lineBreakMode = .byCharWrapping
                    return cell
                }
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "MenuCell") as! MenuCell
                cell.lblTitle.text = menus[indexPath.row]
                cell.lblTitle.lineBreakMode = .byCharWrapping
                return cell
            }
            
        }
    }
    
    
    
    
}
