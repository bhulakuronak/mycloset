//
//  OrderHistoryVC.swift
//  MyCloset
//
//  Created by M on 19/03/18.
//  Copyright © 2018 YDSHaridham. All rights reserved.
//

import UIKit
import Alamofire

class BoughtCell: UITableViewCell {
    @IBOutlet var lblTitle:UILabel!
    @IBOutlet var lblSoldPrice:UILabel!
    @IBOutlet var lblDate:UILabel!
    @IBOutlet var lblCondition:UILabel!
    @IBOutlet var imgProduct:UIImageView!
}

class SoldCell: UICollectionViewCell {
    @IBOutlet var lblTitle:UILabel!
    @IBOutlet var lblSoldPrice:UILabel!
    @IBOutlet var imgProduct:UIImageView!
}

class OrderHistoryVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    var arrBuyProduct = [Any]()
    var arrApplyJob = [Any]()
    var arrPostedJob = [Any]()
    var arrSellingProduct = [Any]()
    
    @IBOutlet var btnBought:UIButton!
    @IBOutlet var btnSold:UIButton!
    
    @IBOutlet var lblEarned:UILabel!
    @IBOutlet var lblSpend:UILabel!
    @IBOutlet var lblTotal:UILabel!
    
    @IBOutlet var tblList:UITableView!
    @IBOutlet var collectionList:UICollectionView!
    @IBOutlet var leadingConstraint:NSLayoutConstraint!
    
    let dateFormatter = DateFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //------Navigation Button------
        self.title = "My Posting"
        self.navigationController?.navigationBar.isTranslucent = false
        //self.navigationController?.navigationBar.barTintColor = Color.COLOR_NAVIGATION_BACKGROUND
        self.addLeftBarMenuWithImage(#imageLiteral(resourceName: "imgMenu").withRenderingMode(.alwaysOriginal))
        
        self.getMyPostingListing()
        
        self.collectionList.isHidden = true
        // Do any additional setup after loading the view.
        
    }

    //MARK: - Methods
    func addLeftBarMenuWithImage(_ buttonImage: UIImage) {
        let leftButton: UIBarButtonItem = UIBarButtonItem(image: buttonImage, style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.toggleLeftMenu))
        navigationItem.leftBarButtonItem = leftButton
    }
    
    @objc public func toggleLeftMenu() {
        slideMenuController()?.toggleLeft()
    }
    
    func getMyPostingListing() {
        
        let userDetail = getMyUserDefaults(key: MyUserDefaults.UserData) as! [String:Any]
        if userDetail == nil {
            appDelegate.showAlertMessage(strMessage: "Please login first.")
            return
        }
        
        
        let parameters = ["i_key":WebURL.appKey,
                          "user_id":userDetail["user_id"] ?? ""] as [String : Any]
        appDelegate.showHud()
        request(WebURL.myPostingAPI, method: .post, parameters: parameters).responseJSON(completionHandler: { (response) in
            
            appDelegate.hideHud()
            if response.result.isSuccess {
                if let dictResult:[String:Any] = response.result.value as! [String : Any]? {
                    print("Get List:\(dictResult)")
                    if (dictResult["o_returnCode"] as! Bool){
                        
                        let arrTemp = dictResult["o_data"] as! [Any]
                        let dictAll = arrTemp[0] as! [String : Any]
                        self.arrBuyProduct = dictAll["buying"] as! [Any]
                        self.arrApplyJob = dictAll["applied_job"] as! [Any]
                        self.arrSellingProduct = dictAll["selling"] as! [Any]
                        self.arrPostedJob = dictAll["posted_job"] as! [Any]
                        self.lblEarned.text = dictAll["earn"] as? String
                        self.lblSpend.text = dictAll["spend"] as? String
                        let earn = dictAll["earn"] as! Int
                        let spend = dictAll["spend"] as! Int
                        self.lblSpend.text = "\(spend)"
                        self.lblEarned.text = "\(earn)"
                        self.lblTotal.text = "\(earn + spend)"
                        
                        self.tblList.reloadData()
                        self.collectionList.reloadData()
                    }else{
                        
                    }
                    
                }
            }else{
                showAlert(msg: "This application requires internet connection", completion: nil)
            }
        })
    }
    
    
    @IBAction func tappedOnTabBar(_ sender:UIButton) {
        if btnBought == sender {
            self.tblList.isHidden = false
            self.collectionList.isHidden = true
            self.leadingConstraint.constant = 0
            UIView.animate(withDuration: 0.8) {
                self.view.layoutIfNeeded()
            }
        }else if btnSold == sender {
            self.tblList.isHidden = true
            self.collectionList.isHidden = false
            self.leadingConstraint.constant = ScreenSize.SCREEN_WIDTH / 2.0
            UIView.animate(withDuration: 0.8) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    //MARK: - UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrBuyProduct.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BoughtCell") as! BoughtCell
        let dictProduct = self.arrBuyProduct[indexPath.row] as! [String:Any]
        cell.lblTitle.text = dictProduct["product_name"] as? String
        cell.lblSoldPrice.text = dictProduct["product_price"] as? String
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = dateFormatter.date(from: dictProduct["product_modified_datetime"] as! String)
        dateFormatter.dateFormat = "dd-MM-yyyy"
        
        cell.lblDate.text = dateFormatter.string(from: date!)
        cell.lblCondition.text = dictProduct["product_status"] as? String
        let strURL = dictProduct["product_cover_image"] as! String
        cell.imgProduct.sd_setImage(with: URL(string: strURL)) { (image, error, nil, url) in
            
        }
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    //MARK: - UICollectionViewDataSource, UICollectionViewDelegate
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrSellingProduct.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SoldCell", for: indexPath) as! SoldCell
        let dictProduct = self.arrSellingProduct[indexPath.row] as! [String:Any]
        cell.lblTitle.text = dictProduct["product_name"] as? String
        cell.lblSoldPrice.text = dictProduct["product_price"] as? String
        let strURL = dictProduct["product_cover_image"] as! String
        cell.imgProduct.sd_setImage(with: URL(string: strURL)) { (image, error, nil, url) in
            
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = ((ScreenSize.SCREEN_WIDTH - 30)/2)
        
        let height = (width * 150.0) / 172.0
        return CGSize(width: width, height: height)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
