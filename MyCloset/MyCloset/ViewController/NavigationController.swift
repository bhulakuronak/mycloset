//
//  NavigationController.swift
//  SolvoList
//
//  Created by ronak patel on 15/09/17.
//  Copyright © 2017 depixed.com. All rights reserved.
//

import UIKit

class NavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationBar.shadowImage = UIImage()
        self.navigationBar.isTranslucent = true
        self.navigationBar.barTintColor = UIColor(patternImage: UIImage(named:"imgTopBar")!)  //Color.COLOR_NAVIGATION_BACKGROUND
        self.navigationBar.tintColor = Color.COLOR_NAVIGATION_TEXT;
        self.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:Color.COLOR_NAVIGATION_TEXT];
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
